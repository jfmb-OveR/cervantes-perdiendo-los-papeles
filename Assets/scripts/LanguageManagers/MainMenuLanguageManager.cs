﻿using UnityEngine;
using System.Collections;
using SmartLocalization;
using UnityEngine.UI;

public class MainMenuLanguageManager : MonoBehaviour {

    public Text tTitulo;
    public Text tSubtitulo;
    public Text tButtonPlay;
    public Text tButtonContinue;
    public Text tButtonContinueDisabled;
    public Text tButtonExit;
    public Text tButtonCredits;
    public Text tTextoLoading;

    private LanguageManager _languageManager;
	// Use this for initialization

    public void setLanguageValues()
    {
        tTitulo.text = _languageManager.GetTextValue("MainMenu.Title");
        tSubtitulo.text = _languageManager.GetTextValue("MainMenu.Subtitle");
        tButtonPlay.text = _languageManager.GetTextValue("MainMenu.Button.Play");
        tButtonContinue.text = _languageManager.GetTextValue("MainMenu.Button.Continuar");
        tButtonContinueDisabled.text = _languageManager.GetTextValue("MainMenu.Button.Continuar");
        tButtonExit.text = _languageManager.GetTextValue("MainMenu.Button.Exit");
        tButtonCredits.text = _languageManager.GetTextValue("MainMenu.Button.Credits");
        tTextoLoading.text = _languageManager.GetTextValue("Auxiliar.Click.Cargando");
    }

    void Awake()
    {
        _languageManager = LanguageManager.Instance;
        _languageManager.ChangeLanguage(myGameManager._sLanguage);

        setLanguageValues();
    }
}
