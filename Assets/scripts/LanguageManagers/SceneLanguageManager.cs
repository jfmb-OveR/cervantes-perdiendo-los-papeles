﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class SceneLanguageManager : MonoBehaviour {
    public Text tBotonPistas;

    [Header("CanvasPistas->AvisoPistas")]
    public Text tTextoAceptarPistas;
    public Text tBotonAceptarAvisoPistas;
    public Text tBotonCerrarAvisoPistas;

    [Header("CanvasPistas->Objetivos")]
    public Text tObjetivos;
    public Text tBotonObjetivos;
    public Text tBotonVerObjetivos;

    [Header("CanvasPistas->PantallaPistas")]
    public Text tTituloMostrarPistas;
    public Text tMostrarPistas;
    public Text tButtonCerrarPistas;
    //    public Text tObjetivos;

    [Header("CanvasDiálogos")]
    public Text tBotonCerrarDialogos;

    [Header("CanvasMenuEscape")]
    public Text tEsc;
    public Text tBotonSi;
    public Text tBotonNo;
    public Text tBotonVolver;


    [Header("Texto de carga")]
    public Text textLoading;

    private LanguageManager _languageManager;
    private int iScene;
    // Use this for initialization
    void Awake()
    {
        _languageManager = LanguageManager.Instance;

        _languageManager.ChangeLanguage(myGameManager._sLanguage);

        textLoading.text = _languageManager.GetTextValue("Auxiliar.Click.Cargando");

        tBotonPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Pistas");
        tBotonCerrarDialogos.text = _languageManager.GetTextValue("Auxiliar.Boton.Cerrar");
        tTextoAceptarPistas.text = _languageManager.GetTextValue("Auxiliar.Aviso.Pistas.Texto");

        tBotonAceptarAvisoPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Aceptar");
        tBotonCerrarAvisoPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Cerrar");

        tTituloMostrarPistas.text = _languageManager.GetTextValue("Auxiliar.Mostrar.Pistas.Titulo");
        tButtonCerrarPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Cerrar");

        tBotonPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Pistas");
        tBotonCerrarDialogos.text = _languageManager.GetTextValue("Auxiliar.Boton.Cerrar");

        tBotonObjetivos.text = _languageManager.GetTextValue("Auxiliar.Boton.Cerrar");
        tBotonVerObjetivos.text = _languageManager.GetTextValue("Auxiliar.Boton.Objetivos");

        tEsc.text = _languageManager.GetTextValue("Auxiliar.Esc.Text");
        tBotonSi.text = _languageManager.GetTextValue("Auxiliar.Esc.Si");
        tBotonNo.text = _languageManager.GetTextValue("Auxiliar.Esc.No");

        tBotonVolver.text = _languageManager.GetTextValue("Auxiliar.Boton.Volver");

        iScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;

        switch (iScene)
        {
            case 4: //Escena 1: Galatea
                tMostrarPistas.text = _languageManager.GetTextValue("Scene01.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene01.Texto.Objetivos");
                break;
            case 7:  //Escena 2.1: Vidriera
                tMostrarPistas.text = _languageManager.GetTextValue("Scene02.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene02.Texto.Objetivos");
                break;
            case 8: //Escena 2.2: Vidriera
                tMostrarPistas.text = _languageManager.GetTextValue("Scene02_2.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene02_2.Texto.Objetivos");
                break;
            case 9: //Escena 2.3: Vidriera
                tMostrarPistas.text = _languageManager.GetTextValue("Scene02_3.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene02_3.Texto.Objetivos");
                break;
            case 12: //Escena 3: Quijote
                tMostrarPistas.text = _languageManager.GetTextValue("Scene03.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene03.Texto.Objetivos");
                break;
            case 15: //Escena 4: 
                tMostrarPistas.text = _languageManager.GetTextValue("Scene04.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene04.Texto.Objetivos");
                break;
            case 18: //Escena 5: 
                tMostrarPistas.text = _languageManager.GetTextValue("Scene05.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene05.Texto.Objetivos");
                break;
            case 21: //Escena 6: Cide Hamete 
                tMostrarPistas.text = _languageManager.GetTextValue("Scene06.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene06.Texto.Objetivos");
                break;
            case 24: //Escena 11: Caballeros
                tMostrarPistas.text = _languageManager.GetTextValue("Scene07.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene07.Texto.Objetivos");
                break;
            case 27: //Escena 12: Barbero
                tMostrarPistas.text = _languageManager.GetTextValue("Scene12.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene12.Texto.Objetivos");
                break;
            case 30: //Escena 12: Maritornes
                tMostrarPistas.text = _languageManager.GetTextValue("Scene13.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene13.Texto.Objetivos");
                break;
            case 33: //Escena Rinconete
                tMostrarPistas.text = _languageManager.GetTextValue("Scene10.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene10.Texto.Objetivos");
                break;
            case 34: //Escena Rinconete
                tMostrarPistas.text = _languageManager.GetTextValue("Scene10.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene10.Texto.Objetivos");
                break;
            case 35: //Escena Rinconete
                tMostrarPistas.text = _languageManager.GetTextValue("Scene10.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene10.Texto.Objetivos");
                break;
            case 38: //Escena Roque
                tMostrarPistas.text = _languageManager.GetTextValue("Scene08.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene08.Texto.Objetivos");
                break;
            case 41: //Escena Roque
                tMostrarPistas.text = _languageManager.GetTextValue("Scene09.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene09.Texto.Objetivos");
                break;
            case 44: //Escena Cipión y Berganza exterior
                tMostrarPistas.text = _languageManager.GetTextValue("Scene11.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene11.Texto.Objetivos");
                break;
            case 45: //Escena Cipión y Berganza hall
                tMostrarPistas.text = _languageManager.GetTextValue("Scene11_2.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene11_2.Texto.Objetivos");
                break;
            case 46: //Escena Cipión y Berganza despacho
                tMostrarPistas.text = _languageManager.GetTextValue("Scene11_3.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene11_3.Texto.Objetivos");
                break;
            case 47: //Escena Cipión y Berganza biblioteca
                tMostrarPistas.text = _languageManager.GetTextValue("Scene11_4.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene11_4.Texto.Objetivos");
                break;
            case 48: //Escena Cipión y Berganza biblioteca
                tMostrarPistas.text = _languageManager.GetTextValue("Scene11_5.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene11_5.Texto.Objetivos");
                break;
            case 49: //Escena Cipión y Berganza biblioteca
                tMostrarPistas.text = _languageManager.GetTextValue("Scene11_6.Mostrar.Pistas.Texto");
                tObjetivos.text = _languageManager.GetTextValue("Scene11_6.Texto.Objetivos");
                break;
        }
    }
}
