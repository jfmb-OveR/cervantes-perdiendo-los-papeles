﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectController : MonoBehaviour {

    public SpriteRenderer Inventario;
//    public SceneManager _scene;
    public ObjectsManager _objectsManager;
    public AudioSource _sound;

    public Vector3 vDestino;
    public float fSpeed;

    private bool bMove = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            //            _scene.IncrementRock();
//            _scene.ActualizarObjetos(tagObjeto);
            //            _scene.ActualizarObjetos(this.tag);
            bMove = true;

//            _scene.IncrementObject();
            _objectsManager.IncrementObject();

            _sound.Play();
        }
    }

    void Update()
    {
        if (bMove == true)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, vDestino, fSpeed * Time.deltaTime);
            if (transform.position == vDestino)
            {
                bMove = false;
                Inventario.enabled = true;
                this.GetComponent<SpriteRenderer>().enabled = false;
                this.GetComponent<Collider2D>().enabled = false;
            }
        }
    }
}
