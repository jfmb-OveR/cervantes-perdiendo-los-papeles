﻿using UnityEngine;
using System.Collections;

public class CloudsMovement : MonoBehaviour {
    public GameObject goInicio;
    public GameObject goFin;

    private float fSpeed;

    public void goPosicionInicial()
    {
        this.transform.position = goInicio.transform.position + new Vector3(0, Random.Range(-1f, 1f), 0);
        fSpeed = Random.Range(0.3f, 1f);
    }

    // Use this for initialization
    void Start () {
        goPosicionInicial();
    }
	
	// Update is called once per frame
	void Update () {
	    if(this.transform.position == goFin.transform.position)
        {
            goPosicionInicial();
        }
        else
            this.transform.position = Vector3.MoveTowards(transform.position, goFin.transform.position, fSpeed * Time.deltaTime);
    }
}
