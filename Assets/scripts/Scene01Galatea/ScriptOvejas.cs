﻿using UnityEngine;
using System.Collections;

public class ScriptOvejas : MonoBehaviour {
    public Transform trDestino;
    public AudioSource[] _sounds;
    public float fSpeed;

    private int iRandom;

	// Update is called once per frame
	void Update () {
        this.transform.position = Vector3.MoveTowards(this.transform.position, trDestino.position, fSpeed * Time.deltaTime);
    }

    void OnMouseDown()
    {
        iRandom = Random.Range(0, _sounds.Length);
        Debug.Log("Número aleatorio: " + iRandom);
        _sounds[iRandom].Play();
    }
}
