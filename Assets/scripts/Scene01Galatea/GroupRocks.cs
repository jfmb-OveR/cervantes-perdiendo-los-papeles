﻿using UnityEngine;
using System.Collections;

public class GroupRocks : MonoBehaviour {

    public SpriteRenderer _imagen;
    public SpriteRenderer _imagen2;
    public SpriteRenderer _fondo1;
    public SpriteRenderer _fondo2;

    public GameObject goBaston;
    public AudioSource _sound;

    public SceneManager _scene;

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (other.tag == "Rocks")
            {
                _imagen.enabled = true;
                _imagen2.enabled = false;
                _fondo1.enabled = false;
                _fondo2.enabled = true;
                goBaston.GetComponent<Collider2D>().enabled = true;
                _sound.Play();
                Destroy(other.gameObject);
            }
            else
            {
                _scene.IncrementErrors(1);
                Debug.Log("No es el objeto que va aquí");
            }
        }
    }

    void Awake()
    {
        goBaston.GetComponent<Collider2D>().enabled = false;
    }
}
