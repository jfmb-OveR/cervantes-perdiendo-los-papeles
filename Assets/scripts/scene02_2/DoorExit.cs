﻿using UnityEngine;
using System.Collections;

public class DoorExit : MonoBehaviour {
    public AudioSource _sonido;
//    public int iLevel;
//    public SceneManager _scene;
    public ObjectsManager _objectManager;
    public SceneManager _scene;
    public CharacterManagerScene01 _character;
    public GameObject goDestino;
    public bool bFlip;

//    public void OnTriggerEnter2D(Collider2D other)
    public void OnMouseDown()
    {
//        if (other.tag == "Player")
        {
//            if(_scene.getNumberOfObjects() >= 2)
            if(_objectManager.getNumberOfObjects() >= 2)
            {
                _character.setDestination(goDestino.transform.position);
                _character.setCharacterMovingParameters();
                _character.setArrived(false);
                if(bFlip == true)
                    _character.characterFlipX();
                _sonido.Play();
                StartCoroutine(WaitBeforeLoading(1));
            }
        }
    }

    IEnumerator WaitBeforeLoading(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        _scene.setIsOverTrue();
//        Application.LoadLevel(iLevel);
    }
}
