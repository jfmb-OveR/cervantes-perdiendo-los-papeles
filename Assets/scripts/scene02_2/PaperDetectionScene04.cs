﻿using UnityEngine;
using System.Collections;

public class PaperDetectionScene04 : MonoBehaviour {
//    public SceneManager _scene;
    public ObjectsManager _objectsManager;
    public AudioSource _soundSuccess;
    public AudioSource _soundError;
    public string cadena;
    public SceneManager _scene;

    public GameObject _destino;
    public float fSpeed;
    public MoveToDestination _moveController;

    private bool bIsOver = false;

//    public void OnTriggerEnter2D(Collider2D other)
    public void OnMouseDown()
    {
        bIsOver = true;
//        _sound.Play();
        if (_objectsManager.getNumberOfObjects() >= 2)
        {

            _moveController.setDestino(_destino.transform.position);
            _moveController.setSpeed(fSpeed);
            _moveController.setMove(true);

            _soundSuccess.Play();
            StartCoroutine(WaitBeforeLoading(1));
        }
        else
        {
            _soundError.Play();
        }
    }

    void Update()
    {
    }

    IEnumerator WaitBeforeLoading(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        _scene.setIsOverTrue();
    }
}

