﻿using UnityEngine;
using System.Collections;

public class ObjectController05 : MonoBehaviour {

    public SpriteRenderer Inventario;
    public SceneManager05 _scene;
    public AudioSource _sound;
    public string sOtherTag;

    public Vector3 vDestino;
    public float fSpeed;
    public string tagObjeto;

    private bool bMove = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == sOtherTag)
        {
            //            _scene.IncrementRock();
            _scene.ActualizarObjetos(tagObjeto);
            //            _scene.ActualizarObjetos(this.tag);
            _sound.Play();
            bMove = true;
        }
    }

    void Update()
    {
        if (bMove == true)
        {
            this.transform.position = Vector3.MoveTowards(transform.position, vDestino, fSpeed * Time.deltaTime);
            if (transform.position == vDestino)
            {
                Inventario.enabled = true;
                this.GetComponent<SpriteRenderer>().enabled = false;
                this.GetComponent<Collider2D>().enabled = false;
            }
        }
    }
}
