﻿using UnityEngine;
using System.Collections;

public class RotateWind : MonoBehaviour {
    public Vector3 vRotation;
    public float fSpeed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Rotate(vRotation * Time.deltaTime * fSpeed);
	}
}
