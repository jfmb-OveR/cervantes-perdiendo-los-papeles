﻿ using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class QuemarLibro : MonoBehaviour {
    public SceneManager05 _scene;
    public string objeto;
    public AudioSource _sound;
    public AudioSource _soundError;
    public GameObject goPapel;
    public GameObject goLibroInventario;
    public SpriteRenderer sLibroQuemado;


    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    [Header("Manager General de la escena")]
    public SceneManager _sceneManager;

    private LanguageManager _languageManager;
    private bool bReached = false;

    private bool bBookBurned;

    void Awake()
    {
        _languageManager = LanguageManager.Instance;
    }

    public bool getBookBurned()
    {
        return bBookBurned;
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (other.tag == objeto)
            {
                if (bReached == false)
                {
                    _scene.ActualizarObjetos(objeto);
//                    goPapel.GetComponent<BoxCollider2D>().enabled = true;
                    goLibroInventario.GetComponent<SpriteRenderer>().sortingLayerName = "Background";
                    goLibroInventario.GetComponent<SpriteRenderer>().sortingOrder = -1;
                    goLibroInventario.GetComponent<DragDropScript>().enabled = false;
                    goLibroInventario.GetComponent<BoxCollider2D>().enabled = false;

                    goPapel.GetComponent<PaperDetectionScene03>().moveBook();

                    Destroy(goLibroInventario);
                    sLibroQuemado.enabled = true;
                    bBookBurned = true;
                    _sound.Play();
                    //            Debug.Log("Libro Quemado");
                }
            }
            else
            {
                if (bReached == false)
                {
                    Debug.Log("Estoy en la hoguera y ha entrado: " + other.tag);
                    _soundError.Play();
                    bReached = true;
                    _sceneManager.IncrementErrors(1);
                    switch (Random.Range(1, 6))
                    {
                        case 1:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                            break;
                        case 2:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                            break;
                        case 3:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                            break;
                        case 4:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                            break;
                        case 5:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                            break;
                    }
                    iImagenError.enabled = true;
                    tTextoError.enabled = true;
                    StartCoroutine(ocultarError());
                }
            }
        }
    }


    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }
    /*
        IEnumerator WaitBeforeLoading(int seconds)
        {
            yield return new WaitForSeconds(seconds);
            Application.LoadLevel(iLevel);
        }
        */

    void Start()
    {
        sLibroQuemado.enabled = false;
        bBookBurned = false;
    }
}
