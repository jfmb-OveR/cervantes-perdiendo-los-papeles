﻿using UnityEngine;
using System.Collections;

public class SceneManager05 : MonoBehaviour {
    public int iNumObjetos;
    public SceneManager _sceneManager;

    private bool[] vObjectosEncontrados;

    public void setSceneOver()
    {
        _sceneManager.setIsOverTrue();
    }

    public void ActualizarObjetos(string objeto)
    {
        switch (objeto)
        {
            case "Hacha":
                vObjectosEncontrados[0] = true;
                Debug.Log("Hacha encontrada");
                break;
            case "Madera":
                vObjectosEncontrados[1] = true;
                break;
            case "MaderaCortada":
                vObjectosEncontrados[2] = true;
                break;
            case "Cristal":
                vObjectosEncontrados[3] = true;
                break;
            case "Libro":
                vObjectosEncontrados[4] = true;
                break;
            case "LibroQuemado":
                vObjectosEncontrados[5] = true;
                break;
        }
    }

    public bool NivelCompleto()
    {
        bool bCompletado = true;
        for (int i = 0; i < iNumObjetos; i++)
        {
//            Debug.Log(i + " Valor: " + vObjectosEncontrados[i]);
            if (vObjectosEncontrados[i] == false)
                bCompletado = false;
        }
        return bCompletado;
    }

	// Use this for initialization
	void Start () {
        vObjectosEncontrados = new bool[iNumObjetos];

        for (int i = 0; i < 5; i++)
        {
            vObjectosEncontrados[i] = false;
        }
	}
}
