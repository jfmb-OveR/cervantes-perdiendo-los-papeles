﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class BreakScript : MonoBehaviour {
    public GameObject objeto;
//    public SpriteRenderer objetoRoto;
    public GameObject objetoRoto;
    [Header("Manager Escena 5")]
    public SceneManager05 _scene;
    public AudioSource _sound;
    public AudioSource _sonidoError;
    [Header("Objeto que colisiona")]
    public string sOtherTag;

    [Header("Objeto colisionado")]
    public string tagObjeto;

//    private bool bMove = false;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    [Header("Manager General Escena")]
    public SceneManager _sceneManager;

    private LanguageManager _languageManager;
    private bool bReached = false;

    void Awake()
    {
        _languageManager = LanguageManager.Instance;
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>()!= null && other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (other.tag == sOtherTag)
            {
                if (bReached == false)
                {
                    objeto.GetComponent<SpriteRenderer>().enabled = false;
                    objeto.GetComponent<BoxCollider2D>().enabled = false;

                    objetoRoto.GetComponent<SpriteRenderer>().enabled = true;
//                    objetoRoto.GetComponent<BoxCollider2D>().enabled = true;
                    StartCoroutine(activarCollider());

                    _scene.ActualizarObjetos(tagObjeto);
                    _sound.Play();

                    bReached = true;
                }
            }
            else
            {
                if (bReached == false)
                {
//                    Debug.Log("He entrado en" + tagObjeto +"  con: " + other.tag);
                    _sonidoError.Play();
                    bReached = true;
                    _sceneManager.IncrementErrors(1);
                    switch (Random.Range(1, 6))
                    {
                        case 1:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                            break;
                        case 2:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                            break;
                        case 3:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                            break;
                        case 4:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                            break;
                        case 5:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                            break;
                    }
                    iImagenError.enabled = true;
                    tTextoError.enabled = true;
                    StartCoroutine(ocultarError());
                }
            }
        }
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(3);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    IEnumerator activarCollider()
    {
        yield return new WaitForSeconds(1.5f);
        objetoRoto.GetComponent<BoxCollider2D>().enabled = true;
    }

    void Start()
    {
        objetoRoto.GetComponent<SpriteRenderer>().enabled = false;
        objetoRoto.GetComponent<BoxCollider2D>().enabled = false;
    }
}
