﻿using UnityEngine;
using System.Collections;

public class PaperDetectionScene03 : MonoBehaviour
{
    //    public SceneManager _scene;
    public ObjectsManager _objectsManager;
    public AudioSource _sound;
    public string cadena;
    public SceneManager _scene;

    public GameObject _destino;
    public float fSpeed;
    public MoveToDestination _moveController;

    public QuemarLibro _libro;

    public SecundarioController _secundario;

    public void moveBook()
    {
        _moveController.setDestino(_destino.transform.position);
        _moveController.setSpeed(fSpeed);
        _moveController.setMove(true);
        _sound.Play();
        _secundario.setContento();
        StartCoroutine(WaitBeforeLoading(1));
    }

    IEnumerator WaitBeforeLoading(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        _scene.setIsOverTrue();
    }

    void Awake()
    {
        _moveController.setMove(false);
        _moveController.setKeepObject(true);
    }
}
