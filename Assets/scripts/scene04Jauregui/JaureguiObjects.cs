﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(MoveToDestination))]
public class JaureguiObjects : MonoBehaviour {
    public PaperDetectionJauregui paperJauregui;
    [Header("Orden de los objetos: Plumas, Huevo, Brasas, Yeso y Agua")]
    public GameObject[] goSprites;

    public AudioSource[] aSonidos;
    public AudioSource _sonidoError;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    public SceneManager _scene;

    public SecundarioController _secundario;

    private LanguageManager _languageManager;
    private bool bReached = false;

    private bool[] bObjects;
    private bool bAllObjects;
    private int iNumObjects = 0;
    private bool bFirstTime = false; //Se usa para ejectuar el movimiento del papel sólo una vez


    public bool getAllObjects()
    {
        return bAllObjects;
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag != "Paper")
        {
//            Debug.Log("El objeto en colisión es: " + other.tag);
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                switch (other.tag)
                {
                    case "Pluma":
                        bObjects[0] = true;
/*
                        goSprites[0].GetComponent<SpriteRenderer>().enabled = false;
                        goSprites[0].GetComponent<SpriteRenderer>().sortingLayerName = "Background";
                        goSprites[0].GetComponent<SpriteRenderer>().sortingOrder = -1;
                        goSprites[0].GetComponent<DragDropScript>().enabled = false;
                        goSprites[0].GetComponent<BoxCollider2D>().enabled = false;
*/
                        goSprites[0].SetActive(false);

                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(goSprites[0]);
                        CheckAllObjects();
                        break;
                    case "Huevo":
                        bObjects[1] = true;
/*                        
                        goSprites[1].GetComponent<SpriteRenderer>().enabled = false;
                        goSprites[1].GetComponent<SpriteRenderer>().sortingLayerName = "Background";
                        goSprites[1].GetComponent<SpriteRenderer>().sortingOrder = -1;
                        goSprites[1].GetComponent<DragDropScript>().enabled = false;
                        goSprites[1].GetComponent<BoxCollider2D>().enabled = false;
*/
                        goSprites[1].SetActive(false);

                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(goSprites[1]);
                        CheckAllObjects();
                        break;
                    case "Brasas":
                        bObjects[2] = true;
/*
                        goSprites[2].GetComponent<SpriteRenderer>().enabled = false;
                        goSprites[2].GetComponent<SpriteRenderer>().sortingLayerName = "Background";
                        goSprites[2].GetComponent<SpriteRenderer>().sortingOrder = -1;
                        goSprites[2].GetComponent<DragDropScript>().enabled = false;
                        goSprites[2].GetComponent<BoxCollider2D>().enabled = false;
*/
                        goSprites[2].SetActive(false);

                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(goSprites[2]);
                        CheckAllObjects();
                        break;
                    case "Yeso":
                        bObjects[3] = true;
/*
                        goSprites[3].GetComponent<SpriteRenderer>().enabled = false;
                        goSprites[3].GetComponent<SpriteRenderer>().sortingLayerName = "Background";
                        goSprites[3].GetComponent<SpriteRenderer>().sortingOrder = -1;
                        goSprites[3].GetComponent<DragDropScript>().enabled = false;
                        goSprites[3].GetComponent<BoxCollider2D>().enabled = false;
*/
                        goSprites[3].SetActive(false);

                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(goSprites[3]);
                        CheckAllObjects();
                        break;
                    case "Agua":
                        bObjects[4] = true;
/*
                        goSprites[4].GetComponent<SpriteRenderer>().enabled = false;
                        goSprites[4].GetComponent<SpriteRenderer>().sortingLayerName = "Background";
                        goSprites[4].GetComponent<SpriteRenderer>().sortingOrder = -1;
                        goSprites[4].GetComponent<DragDropScript>().enabled = false;
                        goSprites[4].GetComponent<BoxCollider2D>().enabled = false;
*/
                        goSprites[4].SetActive(false);
                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(goSprites[4]);
                        CheckAllObjects();
                        break;
                    default:
                        if (bReached == false)
                        {
                            _sonidoError.Play();
                            bReached = true;
                            _scene.IncrementErrors(1);
                            switch (Random.Range(1, 6))
                            {
                                case 1:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                                    break;
                                case 2:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                                    break;
                                case 3:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                                    break;
                                case 4:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                                    break;
                                case 5:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                                    break;
                            }
                            iImagenError.enabled = true;
                            tTextoError.enabled = true;
                            StartCoroutine(ocultarError());
                        }
                        break;
                }
            }
        }
        else
            Debug.Log("Papel en colisión!!!");
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    public bool CheckAllObjects()
    {
        bAllObjects = bObjects[0] && bObjects[1] && bObjects[2] && bObjects[3] && bObjects[4];
//        Debug.Log("Valor de bAllObjects: " + bAllObjects);
        return (bAllObjects);
    }

    // Use this for initialization
    void Awake()
    {
        bAllObjects = false;
        bObjects = new bool[5];
        for (int i=0; i < bObjects.Length; i++)
        {
            bObjects[i] = false;
        }

        _languageManager = LanguageManager.Instance;
    }

    void Update()
    {
        if (CheckAllObjects() == true)
        {
            if (bFirstTime == false)
            {
                bFirstTime = true;
                paperJauregui.movePaper();
                _secundario.setContento();
            }
        }
    }
}
