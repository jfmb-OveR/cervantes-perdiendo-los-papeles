﻿using UnityEngine;
using System.Collections;

public class BucketSprite : MonoBehaviour {
    public Animator anim;
    public AudioSource _soundDrop;

    private int bucketHash = Animator.StringToHash("Bucket");

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Gota")
        {
            _soundDrop.Play();
            anim.SetBool(bucketHash, true);
            StartCoroutine(changeSprite(1f));
        }
    }

    IEnumerator changeSprite(float segundos)
    {
        yield return new WaitForSeconds(segundos);
        anim.SetBool(bucketHash, false);
    }
}
