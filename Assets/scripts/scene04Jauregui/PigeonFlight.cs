﻿using UnityEngine;
using System.Collections;

public class PigeonFlight : MonoBehaviour {

    public Transform trDestino;
    public float fSpeed;
    public Animator anim;

    private Vector3 vDestino;
    public bool bFlight;

    public MoveToDestination _moveScript;

    public AudioSource _pigeonSound;

    public BoxCollider2D colliderPaloma;
    public BoxCollider2D colliderHuevos;

    private int flyingHash = Animator.StringToHash("Flight");
    private bool bPigeonActive = true;

    public void setFlight(bool newValue)
    {
        bFlight = newValue;
//        vDestino = goDestino.transform.position;
    }

    void Awake()
    {
        _moveScript.setDestino(trDestino.position);
        _moveScript.setSpeed(fSpeed);
        _moveScript.setMove(false);
        _moveScript.setKeepObject(false);

        colliderPaloma.enabled = false;
        colliderHuevos.enabled = false;
    }

	// Use this for initialization
	void Start () {
       bFlight = false;
       anim.SetBool(flyingHash, bFlight);

        StartCoroutine(pigeonSound());
    }

    // Update is called once per frame
    void Update () {
	    if (bFlight)
        {
            bPigeonActive = false;
            anim.SetBool(flyingHash, bFlight);
            _moveScript.setMove(true);
/*            this.transform.position = Vector3.MoveTowards(this.transform.position, trDestino.position, fSpeed * Time.deltaTime);
            if (transform.position == vDestino)
            {
                bFlight = false;
                this.enabled = false;
                this.GetComponent<SpriteRenderer>().enabled = false;
            }
*/
        }
    }

    IEnumerator pigeonSound()
    {
        while (bPigeonActive)
        {
            _pigeonSound.Play();
            yield return new WaitForSeconds(5f);
        }
    }
    void OnMouseDown()
    {
        colliderPaloma.enabled = true;
        colliderHuevos.enabled = true;

        setFlight(true);
    }
}
