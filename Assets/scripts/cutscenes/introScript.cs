﻿using UnityEngine;
using System.Collections;
using SmartLocalization;
using UnityEngine.UI;

public class introScript : MonoBehaviour {
    public SpriteRenderer[] vImagenes;
//    public AudioSource[] vNarraciones;
    public Text[] vTextsCutScene;
    [Header("Clave de SmartLoc para el primer texto")]
    public string[] sClavesCadenas;

    [Header("Próxima escena a cargar")]
    public int iNewScene;
    public GameObject goDestino;
    public float fSpeed;

    [Header("Texto de carga")]
    public Text textLoading;

    [Header("Cutscene de inicio de nivel")]
    public bool bIsLevelStart = false;
    [Header("Final cutscene")]
    public bool bIsFinalCutscene = false;
    
    private int counter = 0;
    private int iLength;
    private bool bNextPage;
    private Vector3 vDestino;

    private bool bLoadScene = false;
    // Use this for initialization

    //    [Header("Clave de SmartLoc para el segundo texto")]
    //    public string sNombreCadena2;

    [Header("Texto aviso click")]
    public Text tTextoClick;
    private LanguageManager _languageManager;
    // Use this for initialization
    void Awake()
    {
        _languageManager = LanguageManager.Instance;
        _languageManager.ChangeLanguage(myGameManager._sLanguage);

        tTextoClick.text = _languageManager.GetTextValue("Auxiliar.Click.Screen");

        textLoading.enabled = false;
        textLoading.text = _languageManager.GetTextValue("Auxiliar.Click.Cargando");

        if (vTextsCutScene.Length > 0)
        {
            for (int i = 0; i < vTextsCutScene.Length; i++)
            {
                vTextsCutScene[i].text = _languageManager.GetTextValue(sClavesCadenas[i]);
                vTextsCutScene[i].enabled = false;
            }
        }
        else
            Debug.Log("No se han asociado los textos en el inspector");
    }

    void Start () {
        iLength = vImagenes.Length;
        vDestino = goDestino.transform.position;
        if (iLength > 0)
            vImagenes[0].enabled = true;
//        if (vNarraciones.Length > 0)
//            vNarraciones[0].Play();
        if (vTextsCutScene.Length > 0)
            vTextsCutScene[0].enabled = true;
    }

    // Update is called once per frame
    void Update () {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
        }

        if (bNextPage)
        {
//            vNarraciones[counter].Stop();
            if (vImagenes[counter].transform.position == vDestino)
            {
                bNextPage = false;
                vImagenes[counter].enabled = false;
                vTextsCutScene[counter].enabled = false;

                if ((counter < iLength - 1))
                {
                    counter++;
                    vTextsCutScene[counter].enabled = true;
                }
            }
            else
            {
                vImagenes[counter].transform.position = Vector3.MoveTowards(vImagenes[counter].transform.position, vDestino, fSpeed * Time.deltaTime);
                vTextsCutScene[counter].enabled = false;
//                vTextsCutScene[counter].transform.position = Vector3.MoveTowards(vImagenes[counter].transform.position, vDestino, 2*fSpeed * Time.deltaTime);
            }
        }

        if (Input.anyKeyDown)
        {
            if (counter != iLength - 1)
            {
                bNextPage = true;

            }
            else if (counter == iLength - 1)
            {
                //                Application.LoadLevel(iNewScene);
                int iTempNewScene = iNewScene;

                if ((myGameManager.myGameInstance.gameOver() == true) && (bIsLevelStart == false) && (bIsFinalCutscene == false))
                    iTempNewScene = 54; //Cutscene final

                StartCoroutine(LoadScene(iTempNewScene));
            }
        }
    }

    IEnumerator LoadScene(int newScene)
    {
        bLoadScene = true;
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }

}
