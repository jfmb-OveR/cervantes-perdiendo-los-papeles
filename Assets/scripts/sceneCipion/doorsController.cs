﻿using UnityEngine;
using System.Collections;

public class doorsController : MonoBehaviour
{
    public static bool[] aDoorsOpen; //Puerta izquierda 0, puerta derecha 1
    public static bool bIHaveAKey = false;
    public enterDoors[] aMyDoors;
    public GameObject goKeyInventory;

    public static doorsController myDoorsControllerInstance;

    public bool getIHaveAKey()
    {
        return bIHaveAKey;
    }
    public bool getIsDoorOpen(int iDoor)
    {
        Debug.Log("La puerta " + iDoor + " está abierta? " + aDoorsOpen[iDoor]);
        return aDoorsOpen[iDoor];
    }
    
    public void setIsDoorOpen(int iDoor, bool bValue)
    {
        aDoorsOpen[iDoor] = bValue;
    }

    public void setIHaveAKey(bool bNewValue)
    {
        bIHaveAKey = bNewValue;
    }

    public bool allDoorsOpen()
    {
        return (aDoorsOpen[0] && aDoorsOpen[1]);
    }

    void Awake()
    {
        myDoorsControllerInstance = this;
        if(aDoorsOpen == null)
        {
            aDoorsOpen = new bool[2];
            aDoorsOpen[0] = false;
            aDoorsOpen[1] = false;
        }
    }
    void Start()
    {
        goKeyInventory.GetComponent<SpriteRenderer>().enabled = inventarioCipionHall.bKey;
        goKeyInventory.GetComponent<BoxCollider2D>().enabled = inventarioCipionHall.bKey;
        if (allDoorsOpen() == true)
        {
//            aMyDoors[0].setColliderDisabled();
            aMyDoors[1].setColliderDisabled();
        }
    }
}
