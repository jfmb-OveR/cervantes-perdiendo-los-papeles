﻿using UnityEngine;
using System.Collections;

public class inventarioCipionHall : MonoBehaviour {
    [Header("Key, Longaniza, Libro1, Libro2, Libro3")]
    public GameObject[] goObjetosInventario;

    public static bool bKey = false;
    public static bool bLonganiza = false;
    public static bool bLibro1 = false;
    public static bool bLibro2 = false;
    public static bool bLibro3 = false;

    void Start()
    {
        goObjetosInventario[0].GetComponent<Collider2D>().enabled = bKey;
        goObjetosInventario[0].GetComponent<SpriteRenderer>().enabled = bKey;

        goObjetosInventario[1].GetComponent<Collider2D>().enabled = bLonganiza;
        goObjetosInventario[1].GetComponent<SpriteRenderer>().enabled = bLonganiza;

        goObjetosInventario[2].GetComponent<Collider2D>().enabled = bLibro1;
        goObjetosInventario[2].GetComponent<SpriteRenderer>().enabled = bLibro1;

        goObjetosInventario[3].GetComponent<Collider2D>().enabled = bLibro2;
        goObjetosInventario[3].GetComponent<SpriteRenderer>().enabled = bLibro2;

        goObjetosInventario[4].GetComponent<Collider2D>().enabled = bLibro3;
        goObjetosInventario[4].GetComponent<SpriteRenderer>().enabled = bLibro3;
    }
}
