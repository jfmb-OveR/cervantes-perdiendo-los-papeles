﻿using UnityEngine;
using System.Collections;

public class alguacilController : MonoBehaviour {
    public SceneManager myManager;
    public string sTagCollider;
    public AudioSource soundError;
    public AudioSource soundSuccess01;
    public AudioSource soundSuccess02;

    private Animator alguacilAnimator;
    private int angryHash = Animator.StringToHash("isAngry");
    private int readomgHash = Animator.StringToHash("isReading");

    private enum State
    {
        IDLE,
        READING,
        ANGRY
    }

    private State state;
    private bool bIsReading = false;

    void OnTriggerStay2D(Collider2D other)
    {

        if (other.tag == sTagCollider)
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                state = State.READING;
                alguacilAnimator.SetBool("isReading", true);
                alguacilAnimator.SetBool("isAngry", false);
                Destroy(other.gameObject);
                bIsReading = true;
                soundSuccess01.Play();
            }
        }
    }

    void OnMouseDown()
    {
        if(bIsReading == false)
        {
            state = State.ANGRY;
            alguacilAnimator.SetBool("isAngry", true);
            soundError.Play();
            StartCoroutine(WaitForIdle());
        }
        else
        {
            soundSuccess02.Play();
            myManager.setIsOverTrue();
        }
    }

    IEnumerator WaitForIdle()
    {
        yield return new WaitForSeconds(3);
        state = State.IDLE;
        alguacilAnimator.SetBool("isAngry", false);
    }


    // Use this for initialization
    void Start () {
        state = State.IDLE;
        alguacilAnimator = this.gameObject.GetComponent<Animator>();
        alguacilAnimator.SetBool("isReading", false);
        alguacilAnimator.SetBool("isAngry", false);
    }

    // Update is called once per frame
    void Update () {
	
	}
}
