﻿using UnityEngine;
using System.Collections;

public class enterDoors : MonoBehaviour {
    [Header("0: izquierda, 1: derecha")]
    public int iDoorID;
    public SceneManager mySceneManager;
//    public doorsController myDoorsController;
    public SpriteRenderer spriteDoorClosed;
    public SpriteRenderer spriteDoorOpen;

    [Header("Tag del objeto Llave")]
    public string sOtherTag;
    [Header("Siguiente nivel.Izq: 47, der: 48")]
    public int iNextLevel;

    public AudioSource soundDoor;
    public AudioSource soundKey;
    public AudioSource soundError;

    public bool bIsTheLeftDoor;
    public static bool bBibliotecaVisitada = false;
    public static bool bCocinaVisitada = false;

    public void setColliderDisabled()
    {
        this.GetComponent<BoxCollider2D>().enabled = false;
    }

     public void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if(bIsTheLeftDoor != true)
            {
                if (other.tag == sOtherTag)
                {
                    doorsController.myDoorsControllerInstance.setIsDoorOpen(iDoorID, true);
                    spriteDoorClosed.enabled = false;
                    spriteDoorOpen.enabled = true;
                    soundKey.Play();

                    inventarioCipionHall.bKey = false;
                    Destroy(other.gameObject);
                }
            }
        }
    }

    void OnMouseDown()
    {
        if (bIsTheLeftDoor == false && bCocinaVisitada == false)
        {
            if (doorsController.myDoorsControllerInstance.getIsDoorOpen(iDoorID) == true)
            {
                setColliderDisabled();

                doorsController.myDoorsControllerInstance.setIsDoorOpen(iDoorID, true);
                mySceneManager.LoadNewLevel(iNextLevel, false);
            }
            else
                soundError.Play();
        }
        else if(bBibliotecaVisitada ==  false)
        {
            if (doorsController.myDoorsControllerInstance.getIsDoorOpen(iDoorID) == true)
            {
                setColliderDisabled();

                doorsController.myDoorsControllerInstance.setIsDoorOpen(iDoorID, true);
                mySceneManager.LoadNewLevel(iNextLevel, false);
            }
            else
            {
                doorsController.myDoorsControllerInstance.setIsDoorOpen(iDoorID, true);
                spriteDoorClosed.enabled = false;
                spriteDoorOpen.enabled = true;
                soundDoor.Play();
            }
        }
    }

    void Start()
    {
        bool bDoorOpen = doorsController.myDoorsControllerInstance.getIsDoorOpen(iDoorID);

//        this.GetComponent<BoxCollider2D>().enabled = !bDoorOpen;
        spriteDoorClosed.enabled = !bDoorOpen;
        spriteDoorOpen.enabled = bDoorOpen;
    }

}
