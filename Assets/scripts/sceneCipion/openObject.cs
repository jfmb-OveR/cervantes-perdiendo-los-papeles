﻿using UnityEngine;
using System.Collections;

public class openObject : MonoBehaviour {

    public SpriteRenderer spriteOpen;
    public SpriteRenderer spriteClosed;
    public bool bIsTheMirror;
    public GameObject goKey;
    public AudioSource soundOpen;

    private bool bIsOpen = false;

    void OnMouseDown()
    {
        bIsOpen = !bIsOpen;
        spriteOpen.enabled = bIsOpen;
        spriteClosed.enabled = !bIsOpen;
        soundOpen.Play();
        if (bIsTheMirror == true)
            goKey.SetActive(true);
    }

    void Awake()
    {
        spriteOpen.enabled = bIsOpen;
        spriteClosed.enabled = !bIsOpen;

        if (bIsTheMirror == true)
            goKey.SetActive(false);
    }
}
