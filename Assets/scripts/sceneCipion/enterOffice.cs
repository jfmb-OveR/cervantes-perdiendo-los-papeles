﻿using UnityEngine;
using System.Collections;

public class enterOffice : MonoBehaviour {

    public SceneManager mySceneManager;
    public int iNewScene;

    public static bool bDespachoVisitado = false;    

    void OnMouseDown()
    {
        if(bDespachoVisitado == false)
            mySceneManager.LoadNewLevel(iNewScene, false);
    }
}
