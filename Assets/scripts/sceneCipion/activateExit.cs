﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class activateExit : MonoBehaviour {

    public SpriteRenderer[] arrayOjectsInventory;
    public int iNextLevel;
    public Text textLoading;

    public bool bIsDespacho = false;
    public bool bIsBiblioteca = false;
    public bool bIsCocina = false;

    private bool bLoadScene = false;

    private bool checkObjects()
    {
        bool bAllObjects = true;
        for (int i = 0; (i < arrayOjectsInventory.Length) && (bAllObjects == true); i++)
        {
            bAllObjects = arrayOjectsInventory[i].enabled;
        }

        return bAllObjects;
    }

    void OnMouseDown()
    {
        bLoadScene = checkObjects();
        if (bLoadScene == true)
        {

            StartCoroutine(LoadScene(iNextLevel));
            if (bIsDespacho == true)
            {
                enterOffice.bDespachoVisitado = true; //Variable static alojada en el script de entrada al despacho.
                inventarioCipionHall.bKey = true;
            }
            else if (bIsBiblioteca == true)
            {
                enterDoors.bBibliotecaVisitada = true;
                inventarioCipionHall.bLibro1 = true;
                inventarioCipionHall.bLibro2 = true;
                inventarioCipionHall.bLibro3 = true;
            }
            else if (bIsCocina == true)
            {
                enterDoors.bCocinaVisitada = true;
                inventarioCipionHall.bLonganiza = true;
            }
        }
    }

    IEnumerator LoadScene(int newScene)
    {
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }

    void Update()
    {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
        }
    }
}
