﻿using UnityEngine;
using System.Collections;

public class inventarioRestoSalas : MonoBehaviour {
    [Header("Key, Longaniza, Libro1, Libro2, Libro3")]
    public GameObject[] goObjetosInventario;

    void Start()
    {
        goObjetosInventario[0].GetComponent<Collider2D>().enabled = inventarioCipionHall.bKey;
        goObjetosInventario[0].GetComponent<SpriteRenderer>().enabled = inventarioCipionHall.bKey;

        goObjetosInventario[1].GetComponent<Collider2D>().enabled = inventarioCipionHall.bLonganiza;
        goObjetosInventario[1].GetComponent<SpriteRenderer>().enabled = inventarioCipionHall.bLonganiza;

        goObjetosInventario[2].GetComponent<Collider2D>().enabled = inventarioCipionHall.bLibro1;
        goObjetosInventario[2].GetComponent<SpriteRenderer>().enabled = inventarioCipionHall.bLibro1;

        goObjetosInventario[3].GetComponent<Collider2D>().enabled = inventarioCipionHall.bLibro2;
        goObjetosInventario[3].GetComponent<SpriteRenderer>().enabled = inventarioCipionHall.bLibro2;

        goObjetosInventario[4].GetComponent<Collider2D>().enabled = inventarioCipionHall.bLibro3;
        goObjetosInventario[4].GetComponent<SpriteRenderer>().enabled = inventarioCipionHall.bLibro3;
    }
}
