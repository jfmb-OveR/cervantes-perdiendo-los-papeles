﻿using UnityEngine;
using System.Collections;

public class carpenterController : MonoBehaviour {
    [Header("Tag del objeto válido")]
    public string sOtherTag;

    [Header("Objeto del inventario para activar")]
    public GameObject goObjetoInventario;
    public ParticleSystem particulas;

    public AudioSource _sound;
    public AudioSource soundHammer;
    public AudioSource soundHammer2;

    private enum State
    {
        IDLE,
        WORKING,
        WORKING_NUN
    }

    private State state;

    private Animator anim;

    private bool bIsWorking = false;
    private bool bIsWorkingNun = false;
    private bool bIsOccupied = false;

    private int workingHash = Animator.StringToHash("isWorking");
    private int workingNunHash = Animator.StringToHash("isWorkingNun");

    private bool bReached = false;
    private bool bSoundPlaying = false;
    // Use this for initialization
    void Start()
    {
        anim = this.GetComponent<Animator>();
        state = State.IDLE;

        bIsWorking = bIsOccupied = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (bIsWorking == false && bIsOccupied == false)
        {
            bIsOccupied = true;
            StartCoroutine(carpenterResting());
        }
        else if (bIsWorking == true && bIsOccupied == false)
        {
            bIsOccupied = true;
            StartCoroutine(carpenterWorking());
        }

        switch (state)
        {
            case State.IDLE:
                bIsWorking = false;
                bIsWorkingNun = false;
                //                Debug.Log("Animación descansando!!!!!");
                break;
            case State.WORKING:
                bIsWorking = true;
                bIsWorkingNun = false;

                //                Debug.Log("Animación trabajando!!!!!");
                break;
            case State.WORKING_NUN:
                bIsWorking = false;
                bIsWorkingNun = true;
//                Debug.Log("Animación monja!!!!!");
                break;
        }
        
        anim.SetBool(workingHash, bIsWorking);
        anim.SetBool(workingNunHash, bIsWorkingNun);

    }

    void OnTriggerStay2D(Collider2D other)
    {
        if(state == State.IDLE)
        {
            if (other.tag == sOtherTag)
            {
                if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
                {
                    if (bReached == false)
                    {
                        bReached = true;
                        _sound.Play();
                        goObjetoInventario.GetComponent<SpriteRenderer>().enabled = true;
                        goObjetoInventario.GetComponent<BoxCollider2D>().enabled = true;
                        particulas.Play();
                        state = State.WORKING_NUN;
                        soundHammer.Stop();
                        soundHammer2.Play();

                        StopAllCoroutines();

                        this.GetComponent<BoxCollider2D>().enabled = false;
                        bIsOccupied = true;
                        Destroy(other.gameObject);
                    }
                }
            }
        }
        else
        {
            Debug.Log("Mostrar mensaje de error");
        }
    }

    void OnMouseDown()
    {
    }

    IEnumerator carpenterWorking()
    {
        state = State.WORKING;
        bIsWorking = true;
        soundHammer.Play();
        yield return new WaitForSeconds(5);
        state = State.IDLE;
        soundHammer.Stop();
        bIsWorking = false;
        bIsOccupied = false;
    }

    IEnumerator carpenterResting()
    {
        state = State.IDLE;
        bIsWorking = false;
        yield return new WaitForSeconds(5);
        state = State.WORKING;
        bIsWorking = true;
        bIsOccupied = false;
    }
}
