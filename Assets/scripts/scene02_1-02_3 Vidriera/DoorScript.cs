﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class DoorScript : MonoBehaviour {
    public SpriteRenderer puertaCerrada;
    public GameObject puertaAbierta;
    public GameObject goDestino;
    public bool bFlipCharacter;

    public AudioSource _sonido;
    public AudioSource _sonidoError;
    public AudioSource _sonidoFin;

    public SceneManager _scene;

    public CharacterManagerScene01 _character;

    private bool bDoorOpen = false;
    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    [Header("Tag del objeto con el interactúa")]
    public string stringObjeto;



    private LanguageManager _languageManager;
    private bool bReached = false;
    void Awake()
    {
        _languageManager = LanguageManager.Instance;
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                if (other.tag == stringObjeto)
                {
                    if (bReached == false)
                    {
                        _sonido.Play();
                        puertaCerrada.enabled = false;
                        puertaAbierta.GetComponent<SpriteRenderer>().enabled = true;
                        //            puertaAbierta.GetComponent<Collider2D>().enabled = true;
                        bReached = true;
                        bDoorOpen = true;
                    }
                }
                else
                {
                    if (bReached == false)
                    {
                        _sonidoError.Play();
                        bReached = true;
                        _scene.IncrementErrors(1);
                        switch (Random.Range(1, 6))
                        {
                            case 1:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                                break;
                            case 2:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                                break;
                            case 3:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                                break;
                            case 4:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                                break;
                            case 5:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                                break;
                        }
                        iImagenError.enabled = true;
                        tTextoError.enabled = true;
                        StartCoroutine(ocultarError());
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
            }
        }
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    public void OnMouseDown()
    {
        if(bDoorOpen == true)
        {
            _character.setHayDialogo(false);
            _character.setArrived(false);
            _character.setDestination(goDestino.transform.position);
            _character.setCharacterMovingParameters();

            if (bFlipCharacter == true)
                _character.characterFlipX();
            _sonidoFin.Play();
            StartCoroutine(WaitBeforeLoading(1));
        }
    }

    IEnumerator WaitBeforeLoading(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        _scene.setIsOverTrue();
    }
}
