﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(MoveToDestination))]
public class SkelletonScript : MonoBehaviour {
    public SceneManager _sceneManager;

    public GameObject Inventario;
    public ObjectsManager _objectsManager;
    public AudioSource _sound;
    public ParticleSystem _efectoParticulas;

    public GameObject goDestination;
    public MoveToDestination _movementController;
    public float fSpeed;

    [Header("Destruir el objeto con el que interactúa")]
    public bool bDestroy = false;
    [Header("Tag del objeto con el interactúa")]
    public string stringObjeto;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;
    public AudioSource _soundError;

    private LanguageManager _languageManager;
    private bool bReached = false;

    //    private Vector3 vDestino;

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                if (other.tag == stringObjeto)
                {
                    if (bReached == false)
                    {
                        _objectsManager.IncrementObject();
                        _efectoParticulas.transform.position = goDestination.transform.position;

                        _sound.Play();
                        bReached = true;
                        _movementController.setMove(true);

                        if(bDestroy == true)
                            Destroy(other.gameObject);
                    }
                }
                else
                {
                    if (bReached == false)
                    {
                        bReached = true;
                        StartCoroutine(showError());
                    }
                }
            }
        }
    }

    void OnMouseDown()
    {
        StartCoroutine(showError());
    }

    IEnumerator showError()
    {
        _soundError.Play();
        _sceneManager.IncrementErrors(1);
        switch (Random.Range(1, 6))
        {
            case 1:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                break;
            case 2:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                break;
            case 3:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                break;
            case 4:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                break;
            case 5:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                break;
        }
        iImagenError.enabled = true;
        tTextoError.enabled = true;

        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }


    void Awake()
    {
        _languageManager = LanguageManager.Instance;

        _movementController.setDestino(goDestination.transform.position);
        _movementController.setSpeed(fSpeed);
    }

    void Update()
    {
//        if (this.transform.position == vDestino)
        if (_movementController.getArrived() == true)
        {
            Inventario.GetComponent<SpriteRenderer>().enabled = true;
            Inventario.GetComponent<BoxCollider2D>().enabled = true;
            _efectoParticulas.Play();
            Destroy(this.gameObject);
//            this.GetComponent<SpriteRenderer>().enabled = false;
//            this.GetComponent<Collider2D>().enabled = false;
        }
    }
}
