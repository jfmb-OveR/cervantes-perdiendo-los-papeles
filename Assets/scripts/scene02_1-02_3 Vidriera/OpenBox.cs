﻿using UnityEngine;
using System.Collections;

public class OpenBox : MonoBehaviour {

    public SpriteRenderer imageBoxClosed;
    public SpriteRenderer imageBoxOpen;
    public GameObject goKey;
    public AudioSource _sound;

    void Awake()
    {
        imageBoxClosed.enabled = true;
        goKey.GetComponent<Collider2D>().enabled = false;
        imageBoxOpen.enabled = false;
    }

    public void OnMouseDown()
    {
        imageBoxClosed.enabled = false;
        imageBoxOpen.enabled = true;
        this.GetComponent<Collider2D>().enabled = false;
        goKey.GetComponent<Collider2D>().enabled = true;
        _sound.Play();
    }
}
