﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class VidrieraScript : MonoBehaviour {
//    public SceneManager _manager;
    public ObjectsManager _objectsManager;
    public AudioSource _sonidoCorrecto;
    public AudioSource _sonidoError;
    public SpriteRenderer imagen1;
    public GameObject imagen2;
    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    [Header("Tag del objeto con el interactúa")]
    public string stringObjeto;

    public SceneManager _scene;

    private LanguageManager _languageManager;
    private bool bReached = false;
    void Awake()
    {
        _languageManager = LanguageManager.Instance;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (other.tag == stringObjeto)
            {
                if (bReached == false)
                {
                    imagen1.enabled = false;
                    imagen2.GetComponent<SpriteRenderer>().enabled = true;
                    imagen2.GetComponent<Animator>().enabled = true;
                    _sonidoCorrecto.Play();
                    _objectsManager.IncrementObject();

                    bReached = true;

                    Destroy(other.gameObject);
                    this.GetComponent<Collider2D>().enabled = false;
                }
            }
            else
            {
                if (bReached == false)
                {
                    _sonidoError.Play();
                    bReached = true;
                    _scene.IncrementErrors(1);
                    if (other.tag == "Pliers")
                    {
                        tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error.Vidriera");
                    }
                    else
                    {
                        switch (Random.Range(1, 6))
                        {
                            case 1:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                                break;
                            case 2:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                                break;
                            case 3:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                                break;
                            case 4:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                                break;
                            case 5:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                                break;
                        }
                    }
                    iImagenError.enabled = true;
                    tTextoError.enabled = true;
                    StartCoroutine(ocultarError());
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
        }
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

}
