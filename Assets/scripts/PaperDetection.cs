﻿using UnityEngine;
using System.Collections;

public class PaperDetection : MonoBehaviour {

    public SceneManager _scene;
    public AudioSource _sound;
    public string cadena;
    public DragDropScript _baston;

    public MoveToDestination _moveController;
    public GameObject _destino;
    public float fSpeed;

    public SecundarioController _secundario;

//    private Vector3 vDestino;
//    private bool bMove;

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (other.tag == cadena)
            {
                _moveController.setDestino(_destino.transform.position);
                _moveController.setSpeed(fSpeed);
                _moveController.setMove(true);

                _baston.setUsed(true);
                _sound.Play();
                _secundario.setContento();
                StartCoroutine(WaitBeforeLoading(1));
            }
            else
            {
                _scene.IncrementErrors(1);
                Debug.Log("No es el objeto correcto");
            }
        }
    }

    IEnumerator WaitBeforeLoading(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        _scene.setIsOverTrue();
    }
}
