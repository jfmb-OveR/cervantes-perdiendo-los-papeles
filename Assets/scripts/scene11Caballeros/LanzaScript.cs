﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(MoveToDestination))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class LanzaScript : MonoBehaviour {
    public MoveToDestination _objectMovement;
    public GameObject goDestination;

    public float fSpeed = 30;
    public AudioSource _soundSuccess;
    public AudioSource _soundError;

    [Header("Tag con el que va a coger el objeto")]
    public string sNewTag;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    public SceneManager _scene;

    public ParticleSystem _particleSystem;

    private LanguageManager _languageManager;

    private bool bReached = false;

    private bool bIsUsed = false;

    // Use this for initialization
    void Awake ()
    {
        _languageManager = LanguageManager.Instance;

        _objectMovement.setDestino(goDestination.transform.position);
        _objectMovement.setSpeed(fSpeed);
        _objectMovement.setMove(false);
//        _objectMovement.setKeepObject(false);
    }
	
	// Update is called once per frame
	void Update () {
	    if((_objectMovement.getArrived() == true) && (bIsUsed == false))
        {
            this.GetComponent<SpriteRenderer>().enabled = false;
            this.GetComponent<BoxCollider2D>().enabled = false;
            Destroy(this.gameObject);
            goDestination.GetComponent<SpriteRenderer>().enabled = true;
            goDestination.GetComponent<BoxCollider2D>().enabled = true;
            _particleSystem.transform.position = goDestination.transform.position;
            _particleSystem.Play();
            bIsUsed = true;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (other.tag == sNewTag)
            {
                if (bReached == false)
                {
                    _soundSuccess.Play();
                    _objectMovement.setMove(true);
                    Destroy(other.gameObject);

                    bReached = true;
                }
            }
            else
            {
                if (bReached == false)
                {
                    _soundError.Play();
                    bReached = true;
                    _scene.IncrementErrors(1);
                    switch (Random.Range(1, 6))
                    {
                        case 1:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                            break;
                        case 2:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                            break;
                        case 3:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                            break;
                        case 4:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                            break;
                        case 5:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                            break;
                    }
                    iImagenError.enabled = true;
                    tTextoError.enabled = true;
                    StartCoroutine(ocultarError());
                }
            }
        }
    }

    void OnMouseDown()
    {
        _soundError.Play();
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }
}
