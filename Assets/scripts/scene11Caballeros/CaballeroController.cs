﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class CaballeroController : MonoBehaviour
{
    [Header("Caballero luna: Lanza")]
    [Header("Caballero verde: Alfanje")]
    public string sNewTag;

    [Header("Caballero luna: true")]
    [Header("Caballero verde: false")]
    public bool bNewObject = false;

    public GameObject goObjeto;
    public GameObject goObjetoInventario;
    public float fSpeed = 30;

    public Animator anim;

    [Header("Script de la cámara para acabar")]
    public SceneManager _scene;

    [Header("Sonidos de éxito y fracaso")]
    public AudioSource _soundSuccess;
    public AudioSource _soundError;
    public ParticleSystem _efectoParticulas;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    private LanguageManager _languageManager;


    private bool bIsSatisfied = false;
    private int satisfiedHash = Animator.StringToHash("isSatisfied");
    private bool bReached = false;
    private enum State
    {
        IDLE1,
        IDLE2
    }

    private State _state;

    // Use this for initialization
    void Awake()
    {
        _languageManager = LanguageManager.Instance;

        anim = this.GetComponent<Animator>();
        _state = State.IDLE1;

        if(bNewObject == true)
        {
            goObjeto.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            Destroy(goObjetoInventario);
            Destroy(goObjeto);
        }
    }

    // Update is called once per frame
    void Update()
    {
        switch (_state)
        {
            case State.IDLE1:
                bIsSatisfied = false;
                break;
            case State.IDLE2:
                bIsSatisfied = true;
                break;
        }
        anim.SetBool(satisfiedHash, bIsSatisfied);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                if (other.tag == sNewTag)
                {
                    if (bReached == false)
                    {
                        _state = State.IDLE2;
                        _soundSuccess.Play();
                        Destroy(other.gameObject);
                        if (bNewObject == true)
                        {
                            goObjeto.GetComponent<MoveToDestination>().setDestino(goObjetoInventario.transform.position);
                            goObjeto.GetComponent<MoveToDestination>().setSpeed(fSpeed);
                            goObjeto.GetComponent<MoveToDestination>().setMove(true);

                            goObjetoInventario.GetComponent<SpriteRenderer>().enabled = true;
                            goObjetoInventario.GetComponent<BoxCollider2D>().enabled = true;
                            _efectoParticulas.Play();
                        }
                        else
                        {
                            StartCoroutine(EndLevel());
                        }
                    }
                }
                else
                {
                    if (bReached == false)
                    {
                        _soundError.Play();
                        bReached = true;
                        _scene.IncrementErrors(1);
                        switch (Random.Range(1, 6))
                        {
                            case 1:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                                break;
                            case 2:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                                break;
                            case 3:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                                break;
                            case 4:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                                break;
                            case 5:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                                break;
                        }
                        iImagenError.enabled = true;
                        tTextoError.enabled = true;
                        StartCoroutine(ocultarError());
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
            }
        }
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    IEnumerator EndLevel()
    {
        yield return new WaitForSeconds(2);
        _scene.setIsOverTrue();
    }

    void OnMouseDown()
    {
        _soundError.Play();
    }
}
