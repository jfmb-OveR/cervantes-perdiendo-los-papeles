﻿using UnityEngine;
using System.Collections;


[DisallowMultipleComponent]
public class PlayerMovement : MonoBehaviour
{
    public CharacterController _controller;
    public SpriteRenderer _mySprite;

    [SerializeField]
    [Range(1, 20)]
    private float speed = 10;                   //how fast the player moves.

    private Vector3 targetPosition;             //where we want to travel too.
    private bool isMoving;                      //toggle to check track if we are moving or not.

//    public float fTotalLeftLimit = -9.5f;
//    public float fTotalRightLimit = 9.5f;
    public float fRightLimit;
    public float fUpLimit;

    const int LEFT_MOUSE_BUTTON = 0;            //a more visual description of what the left mouse button code is.

//    public bool upLimitOK(float x, float y)
//    {
//        return true;
//    }

    /// <summary>
    /// Use this for initialization.
    /// </summary>
    void Start()
    {
        targetPosition = transform.position;        //set the target postion to where we are at the start
        isMoving = false;                           //set  out move toggle to false.
        _controller.setStateIDLE();
    }

    /// <summary>
    /// Detect the player input every frame
    /// </summary>
    void Update()
    {
/*
            if (Input.GetMouseButton(0))
            {
                var targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                targetPos.z = transform.position.z;
                transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
            }
*/
        //if the player clicked on the screen, find out where
        if (Input.GetMouseButton(LEFT_MOUSE_BUTTON))
        {
            SetTargetPosition();
        }

        //if we are still moving, then move the player
        if (isMoving)
            MovePlayer();
    }

    public void setPosition()
    {
        targetPosition.x = transform.position.x;
        targetPosition.y = transform.position.y;
    }

    /// <summary>
    /// Sets the target position we will travel too.
    /// </summary>
    void SetTargetPosition()
    {

        targetPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        targetPosition.z = transform.position.z;
        if(_controller.getIsBlocked() == false)
        {
            if ((targetPosition.x <= fRightLimit) && (targetPosition.y <= fUpLimit))
            {
//                Debug.Log("Origen: " + transform.position);
                Debug.Log("Destino: " + targetPosition);
                if (transform.position.x > targetPosition.x)
                {
                    _mySprite.flipX = true;
                }
                else
                    _mySprite.flipX = false;

                //set the ball to move
                isMoving = true;
                _controller.setStateWALKING();
            }
        }
    }



    /// <summary>
    /// Moves the player in the right direction and also rotates them to look at the target position.
    /// When the player gets to the target position, stop them from moving.
    /// </summary>
    void MovePlayer()
    {
//        transform.LookAt(targetPosition);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        //if we are at the target position, then stop moving
        if (transform.position == targetPosition)
        {
            isMoving = false;
            _controller.setStateIDLE();
        }

        Debug.DrawLine(transform.position, targetPosition, Color.red);
    }
}