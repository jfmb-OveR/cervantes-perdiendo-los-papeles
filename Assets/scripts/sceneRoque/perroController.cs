﻿using UnityEngine;
using System.Collections;

public class perroController : MonoBehaviour {
    public SceneManager _sceneManager;

    [Header("Animator del ventero")]
    public Animator anim;

    [Header("Collider de las pistolas")]
    public BoxCollider2D colliderGuns;
    public AudioSource _soundError;


    //    [Header("Sonidos del Ventero")]
    //    public AudioSource _soundGatoNormal;
    //    public AudioSource _soundGatoEnfadado;

    private enum State
    {
        IDLE,
        ANGRY,
        DRINK
    }

    private State state;

    private bool bIsAngry;
    private int AngryHash = Animator.StringToHash("isAngry");

    private bool bIsDrinking;
    private int DrinkHash = Animator.StringToHash("isDrinking");


    public void setAngryState()
    {
        StartCoroutine(setAngry());
    }

    public void setDrinkState()
    {
        state = State.DRINK;
    }

    // Use this for initialization
    void Start()
    {
        state = State.IDLE;

        colliderGuns.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        switch (state)
        {
            case State.IDLE:
                //                    Debug.Log("ESTADO IDLE");
                //                _catMovement.setMove(false);
                bIsAngry = false;
                bIsDrinking = false;
                break;
            case State.ANGRY:
                //                    Debug.Log("ESTADO WALKING   ");
                bIsAngry = true;
                bIsDrinking = false;
                break;
            case State.DRINK:
                //                    Debug.Log("ESTADO ANGRY   ");
                bIsAngry = false;
                bIsDrinking = true;
                break;
        }
        anim.SetBool(AngryHash, bIsAngry);
        anim.SetBool(DrinkHash, bIsDrinking);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        //        Debug.Log(other.tag + "!!!!!!!");
        if (other.tag != "Untagged")
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                if(other.tag == "Agua")
                {
                    setDrinkState();
                    Destroy(other.gameObject);
                    this.GetComponent<BoxCollider2D>().enabled = false;
                    colliderGuns.enabled = true;
                }
                else
                    setAngryState();
            }
    }

    void OnMouseDown()
    {
        Debug.Log("He pulsado en el perro");
        setAngryState();
    }

    IEnumerator setAngry()
    {
        _soundError.Play();
        _sceneManager.IncrementErrors(1);
        state = State.ANGRY;
        yield return new WaitForSeconds(1);
        state = State.IDLE;
    }
}
