﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class barrelScript : MonoBehaviour {
    [Header("Objetos del inventario: Cubo, tela, arena")]
    public GameObject[] goSprites;
    [Header("Nuevos sprites del cubo: con agua, arena y tela")]
    public SpriteRenderer[] spritesNewBucketSprites;
    [Header("Tags de los objetos del inventario")]
    public string[] arrayTags;
    [Header("Sonidos de éxito")]
    public AudioSource[] aSonidos;
    public AudioSource _sonidoError;

    [Header("Barril en el inventario")]
    public GameObject goCuboInventario;
    public ParticleSystem particulas;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    public SceneManager _scene;

    private LanguageManager _languageManager;
    private bool bReached = false;

    private bool[] bObjects;
    private bool bAllObjects;
    private int iNumObjects = 0;

    private MoveToDestination scriptMoveToDestination;
    //    private bool bFirstTime = false; //Se usa para ejectuar el movimiento del papel sólo una vez

    private Sprite mySprite;

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                switch (other.tag)
                {
                    case "Cubo":
                        if (bObjects[1] == true && bObjects[2] == true)
                        {
                            bObjects[0] = true;
                            goSprites[0].SetActive(false);

                            spritesNewBucketSprites[0].enabled = true;
                            mySprite = spritesNewBucketSprites[0].sprite;
                            this.GetComponent<SpriteRenderer>().sprite = mySprite;

                            aSonidos[iNumObjects].Play();
                            iNumObjects++;
                            Destroy(other.gameObject);
                            spritesNewBucketSprites[0].enabled = false;
                            spritesNewBucketSprites[1].enabled = false;
                            spritesNewBucketSprites[2].enabled = false;


//                            Destroy(spritesNewBucketSprites[0]);
//                            Destroy(spritesNewBucketSprites[1]);
//                            Destroy(spritesNewBucketSprites[2]);
                            //                    Destroy(goSprites[0]);
                            //                            CheckAllObjects();
                            this.GetComponent<barrelScript>().enabled = false;
                            Destroy(this);

                            scriptMoveToDestination.setMove(true);
                            goCuboInventario.GetComponent<SpriteRenderer>().enabled = true;
                            goCuboInventario.GetComponent<BoxCollider2D>().enabled = true;
                            particulas.Play();
                            this.GetComponent<SpriteRenderer>().enabled = false;
                        }
                        else
                        {
                            bReached = true;
                            StartCoroutine(showError());
                        }
                        break;
                    case "Tela":
                        bObjects[1] = true;
                        goSprites[1].SetActive(false);

                        spritesNewBucketSprites[1].enabled = true;
                        mySprite = spritesNewBucketSprites[1].sprite;

                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(other.gameObject);
                        //                    Destroy(goSprites[1]);
                        //                    CheckAllObjects();
                        break;
                    case "Arena":
                        bObjects[2] = true;
                        goSprites[2].SetActive(false);

                        spritesNewBucketSprites[2].enabled = true;
                        mySprite = spritesNewBucketSprites[2].sprite;

                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(other.gameObject);

                        //                    Destroy(goSprites[2]);
                        //                    CheckAllObjects();
                        break;
                    default:
                        if (bReached == false)
                        {
                            bReached = true;
                            StartCoroutine(showError());
                        }
                        break;
                }
            }
        }
    }

    IEnumerator showError()
    {
        _sonidoError.Play();
        _scene.IncrementErrors(1);
        switch (Random.Range(1, 6))
        {
            case 1:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                break;
            case 2:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                break;
            case 3:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                break;
            case 4:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                break;
            case 5:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                break;
        }
        iImagenError.enabled = true;
        tTextoError.enabled = true;

        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    /*
        IEnumerator ocultarError()
        {
            yield return new WaitForSeconds(4);
            iImagenError.enabled = false;
            tTextoError.enabled = false;
            bReached = false;
        }
    */

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Untagged")
            other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
    }

    public bool CheckAllObjects()
    {
        bAllObjects = bObjects[0] && bObjects[1] && bObjects[2];
        //        Debug.Log("Valor de bAllObjects: " + bAllObjects);
        return (bAllObjects);
    }

    void Awake()
    {
        scriptMoveToDestination = this.GetComponent<MoveToDestination>();
        scriptMoveToDestination.setDestino(goCuboInventario.transform.position);
        scriptMoveToDestination.setSpeed(30f);
        scriptMoveToDestination.setMove(false);

        mySprite = this.GetComponent<SpriteRenderer>().sprite;

        bObjects = new bool[3];
        for (int i = 0; i < bObjects.Length; i++)
        {
            spritesNewBucketSprites[i].enabled = false;
            bObjects[i] = false;
        }

        _languageManager = LanguageManager.Instance;

    }
}
