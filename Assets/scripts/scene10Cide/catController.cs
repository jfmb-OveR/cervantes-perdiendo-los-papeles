﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveToDestination))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]

public class catController : MonoBehaviour {

    //    public RandomMovement _horseMovement;
    [Header("Configuración del gato")]
    public MoveToDestination _catMovement;
    public GameObject goDestination;
    [Header("Tag del objeto válido")]
    public string sOtherTag;

    public float fSpeed;
    public AudioSource _sound;

    [Header("Asta de ciervo")]
    public GameObject goAstaCiervo;

    [Header("Animator del gato")]
    public Animator anim;

    [Header("Maullidosdel gato")]
    public AudioSource _soundGatoNormal;
    public AudioSource _soundGatoEnfadado;

    private bool bReached = false;

    private enum State
    {
        IDLE,
        WALKING,
        ANGRY
    }

    private State state;

    private bool bIsWalking;
    private int walkingHash = Animator.StringToHash("isWalking");

    private bool bIsAngry;
    private int AngryHash = Animator.StringToHash("isAngry");

    private int waypointIndex;
    private int randomNumber;

    // Use this for initialization
    void Start()
    {
        state = State.IDLE;

        _catMovement.setDestino(goDestination.transform.position);
        _catMovement.setSpeed(fSpeed);
        _catMovement.setMove(false);
        _catMovement.setKeepObject(false);

        goAstaCiervo.GetComponent<BoxCollider2D>().enabled = false;

        _soundGatoNormal.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (_catMovement.getArrived() == true)
        {
            state = State.IDLE;
        }

        switch (state)
        {
            case State.IDLE:
//                    Debug.Log("ESTADO IDLE");
                _catMovement.setMove(false);
                bIsAngry = false;
                bIsWalking = false;
                break;
            case State.WALKING:
//                    Debug.Log("ESTADO WALKING   ");
                bIsAngry = false;
                bIsWalking = true;
                goAstaCiervo.GetComponent<BoxCollider2D>().enabled = true;
                break;
            case State.ANGRY:
//                    Debug.Log("ESTADO ANGRY   ");
                bIsAngry = true;
                bIsWalking = false;
                break;
        }
        anim.SetBool(AngryHash, bIsAngry);
        anim.SetBool(walkingHash, bIsWalking);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (other.tag == sOtherTag)
            {
                if (bReached == false)
                {
                    Destroy(other.gameObject); 
                    StartCoroutine(catEscape());
                }
            }
        }
    }

    void OnMouseDown()
    {
        state = State.ANGRY;
        _soundGatoNormal.Stop();
        _soundGatoEnfadado.Play();
        StartCoroutine(catAngry());
    }

    IEnumerator catAngry()
    {
        _soundGatoNormal.Stop();
        _soundGatoNormal.Play();
        yield return new WaitForSeconds(1);
        state = State.IDLE;
    }

    IEnumerator catEscape()
    {
        state = State.ANGRY;
        _soundGatoNormal.Stop();
        _soundGatoEnfadado.Play();
        yield return new WaitForSeconds(.5f);
        state = State.WALKING;
        yield return new WaitForSeconds(.5f);
        _catMovement.setMove(true);
    }
}
