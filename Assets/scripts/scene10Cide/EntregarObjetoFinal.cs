﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class EntregarObjetoFinal : MonoBehaviour {
    [Header("Manager para finalizar la escena")]
    public SceneManager _sceneManager;

    [Header("Tag Poema")]
    public string sTag;

    public AudioSource _sound;
    public AudioSource _sonidoError;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    [Header("Mover el papel a Cervantes")]
    public MoveToDestination _paperMovement;
    public GameObject goDestination;
    public float fSpeed = 10f;

    public SceneManager _scene;

    public SecundarioController _secundario;

    private LanguageManager _languageManager;

    private bool bReached = false;
    //    private Vector3 vDestino;

    public void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                if (other.tag == sTag)
                {
                    if (bReached == false)
                    {
                        //            Debug.Log("Llave entregada");
                        Destroy(other.gameObject);
                        _sound.Play();
                        _secundario.setContento();
                        _paperMovement.setMove(true);
                        StartCoroutine(EndLevel());
                        bReached = true;
                    }
                }
                else
                {
                    if (bReached == false)
                    {
                        _sonidoError.Play();
                        bReached = true;
                        _scene.IncrementErrors(1);
                        switch (Random.Range(1, 6))
                        {
                            case 1:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                                break;
                            case 2:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                                break;
                            case 3:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                                break;
                            case 4:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                                break;
                            case 5:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                                break;
                        }
                        iImagenError.enabled = true;
                        tTextoError.enabled = true;
                        StartCoroutine(ocultarError());
                    }
                }
            }
        }
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }


    // Use this for initialization
    void Awake()
    {      
        _paperMovement.setDestino(goDestination.transform.position);
        _paperMovement.setSpeed(fSpeed);
        _paperMovement.setMove(false);

        _languageManager = LanguageManager.Instance;
    }

    IEnumerator EndLevel()
    {
        yield return new WaitForSeconds(1);
        _sceneManager.setIsOverTrue();
    }
}
