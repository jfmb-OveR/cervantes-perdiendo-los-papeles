﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdsSinging : MonoBehaviour {

    public AudioSource _bird01;
    public AudioSource _bird02;

    public float fMaxDelayBird01 = 10f;
    public float fMaxDelayBird02 = 10f;

    private bool bBird01Singing = false;
    private bool bBird02Singing = false;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (bBird01Singing == false)
        {
            bBird01Singing = true;
            StartCoroutine(Bird01(Random.Range(0f, fMaxDelayBird01)));
        }
        if (bBird02Singing == false)
        {
            bBird02Singing = true;
            StartCoroutine(Bird02(Random.Range(0f, fMaxDelayBird02)));
        }
    }

    IEnumerator Bird01(float segundos)
    {
        _bird01.Play();
        yield return new WaitForSeconds(segundos);
        bBird01Singing = false;
    }

    IEnumerator Bird02(float segundos)
    {
        _bird02.Play();
        yield return new WaitForSeconds(segundos);
        bBird02Singing = false;
    }

}
