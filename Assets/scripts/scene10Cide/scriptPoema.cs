﻿using UnityEngine;
using System.Collections;

public class scriptPoema : MonoBehaviour {
    [Header("Script del objeto que tiene encima")]
    public cambiarObjeto _cambiarObjeto;
    private bool bCambiado;
    // Use this for initialization

    void Awake()
    {
        this.GetComponent<Collider2D>().enabled = false;
    }

    void Start () {
        bCambiado = false;
	}
	
	// Update is called once per frame
	void Update () {
	    if (bCambiado == false)
        {
            if (_cambiarObjeto.getObjetoCambiado() == true)
            {
                this.GetComponent<Collider2D>().enabled = true;
                bCambiado = true;
            }
        }
	}
}
