﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class barberoController : MonoBehaviour {
    //    public SceneManager _manager;
    public ObjectsManager _objectsManager;
    public AudioSource _sonidoCorrecto;
    public AudioSource _sonidoError;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    [Header("Tag del objeto con el interactúa")]
    public string stringNavaja;
    public string stringYelmo;

    public SceneManager _scene;

    public Animator anim;

    private bool bConNavaja = false;
    private bool bConYelmo = false;

    private int navajaHash = Animator.StringToHash("conNavaja");
    private int yelmoHash = Animator.StringToHash("conYelmo");

    private LanguageManager _languageManager;
    private bool bReached = false;

    void Awake()
    {
        _languageManager = LanguageManager.Instance;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                if (bReached == false)
                {
                    if (other.tag == stringNavaja)
                    {
                        _sonidoCorrecto.Play();
                        _objectsManager.IncrementObject();

//                        bReached = true;
                        bConNavaja = true;
                        Destroy(other.gameObject);
                        //                            this.GetComponent<Collider2D>().enabled = false;
                    }
                    else if (other.tag == stringYelmo)
                    {
                        _sonidoCorrecto.Play();
                        _objectsManager.IncrementObject();

//                        bReached = true;
                        bConYelmo = true;
                        Destroy(other.gameObject);
                        //                            this.GetComponent<Collider2D>().enabled = false;
                    }
                    else
                    {
                        _sonidoError.Play();
                        bReached = true;
                        _scene.IncrementErrors(1);
                        if(other.tag == "YelmoSucio")
                        {
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error.Barbero02");
                        }
                        else if (other.tag == "Llave")
                        {
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error.Barbero01");
                        }
                        else
                        {
                            switch (Random.Range(1, 6))
                            {
                                case 1:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                                    break;
                                case 2:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                                    break;
                                case 3:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                                    break;
                                case 4:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                                    break;
                                case 5:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                                    break;
                            }
                        }
                        iImagenError.enabled = true;
                        tTextoError.enabled = true;
                        StartCoroutine(ocultarError());
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                Debug.Log("Sale el objeto: " + other.tag);
                other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                Debug.Log("Sale el objeto: " + other.tag);
                other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
            }
        }
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    public bool estaArmado()
    {
        return ((bConNavaja == true) && (bConYelmo == true));
    }

    // Use this for initialization
    void Start () {
        anim.SetBool(navajaHash, bConNavaja);
        anim.SetBool(yelmoHash, bConYelmo);
    }

    // Update is called once per frame
    void Update () {
        anim.SetBool(navajaHash, bConNavaja);
        anim.SetBool(yelmoHash, bConYelmo);
    }
}
