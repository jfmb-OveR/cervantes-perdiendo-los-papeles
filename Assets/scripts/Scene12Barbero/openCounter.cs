﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(BoxCollider2D))]
public class openCounter : MonoBehaviour {

    public SpriteRenderer objetoAbierto;
    public GameObject goNavaja;
	// Use this for initialization
	void Awake () {
        objetoAbierto.enabled = false;
        goNavaja.GetComponent<SpriteRenderer>().enabled = false;
        goNavaja.GetComponent<BoxCollider2D>().enabled = false;
    }

    // Update is called once per frame
    void Update () {
	
	}

    public void OnMouseDown()
    {
        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;

        objetoAbierto.enabled = true;
        goNavaja.GetComponent<SpriteRenderer>().enabled = true;
        goNavaja.GetComponent<BoxCollider2D>().enabled = true;
    }
}
