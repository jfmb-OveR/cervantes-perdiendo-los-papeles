﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class openCage : MonoBehaviour {
    [Header("Primer objecto en pantalla")]
    public GameObject objeto;
    //    public SpriteRenderer objetoRoto;
    [Header("Objecto que va a sustituir al primero")]
    public GameObject otroObjecto;
    public string sOtherTag;
    //    public SceneManager05 _scene;

    public AudioSource _soundSuccess;
    public AudioSource _soundError;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    [Header("Controlador del león")]
    public lionController leon;
    public barberoController barbero;
    public SceneManager _scene;

    public ParticleSystem _particleSystem;

    private LanguageManager _languageManager;
    private bool bReached = false;

    private bool bObjetoCambiado;

    //    private bool bMove;

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                if (other.tag == sOtherTag)
                {
                    if (bReached == false)
                    {
                        if (barbero.estaArmado() == true)
                        {
                            objeto.GetComponent<SpriteRenderer>().enabled = false;
                            objeto.GetComponent<BoxCollider2D>().enabled = false;

                            otroObjecto.GetComponent<SpriteRenderer>().enabled = true;
//                            otroObjecto.GetComponent<BoxCollider2D>().enabled = true;

                            bObjetoCambiado = true;

                            Destroy(other.gameObject);

                            leon.startRunning();
                            _soundSuccess.Play();
                            //            bMove = true;
                        }
                        else
                        {
                            _soundError.Play();
                            bReached = true;
                            _scene.IncrementErrors(1);

                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error.Barbero03");
                            iImagenError.enabled = true;
                            tTextoError.enabled = true;
                            StartCoroutine(ocultarError());
                        }
                    }
                    else
                    {
                        if (bReached == false)
                        {
                            _soundError.Play();
                            bReached = true;
                            _scene.IncrementErrors(1);
                            switch (Random.Range(1, 6))
                            {
                                case 1:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                                    break;
                                case 2:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                                    break;
                                case 3:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                                    break;
                                case 4:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                                    break;
                                case 5:
                                    tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                                    break;
                            }
                            iImagenError.enabled = true;
                            tTextoError.enabled = true;
                            StartCoroutine(ocultarError());
                        }
                    }

                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag != "Untagged")
            other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Untagged")
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
                other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    void OnMouseDown()
    {
        _soundError.Play();
    }


    public bool getObjetoCambiado()
    {
        return (bObjetoCambiado);
    }

    void Awake()
    {
        _languageManager = LanguageManager.Instance;

        otroObjecto.GetComponent<SpriteRenderer>().enabled = false;
//        otroObjecto.GetComponent<BoxCollider2D>().enabled = false;

    }

}
