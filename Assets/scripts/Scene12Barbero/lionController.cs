﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class lionController : MonoBehaviour {

    public Transform tDestination;
    public float fSpeed = 10f;

    public SceneManager _scene;

    private MoveToDestination moveLion;

    private Animator anim;

    private bool bIsRoaring = false;
    private int roaringHash = Animator.StringToHash("isRoaring");

    private bool bIsRunning = false;
    private int runningHash = Animator.StringToHash("isRunning");

    void OnMouseDown()
    {
        Debug.Log("He entrado en el MouseDown");
        bIsRoaring = true;
        anim.SetBool(roaringHash, bIsRoaring);
    }

    void OnMouseUp()
    {
        Debug.Log("He entrado en el MouseUP");
        bIsRoaring = false;
        anim.SetBool(roaringHash, bIsRoaring);
    }

    public void startRunning()
    {
        bIsRoaring = false;
        anim.SetBool(roaringHash, bIsRoaring);

        bIsRunning = true;
        anim.SetBool(runningHash, bIsRunning);

        moveLion.setMove(true);

        this.GetComponent<SpriteRenderer>().sortingOrder = 6;

        StartCoroutine(endScene());
    }

    void Awake()
    {
        moveLion = this.GetComponent<MoveToDestination>();
    }

    // Use this for initialization
    void Start () {
        moveLion.setDestino(tDestination.position);
        moveLion.setSpeed(fSpeed);
        moveLion.setMove(false);

        anim = this.GetComponent<Animator>();
	}

    IEnumerator endScene()
    {
        yield return new WaitForSeconds(1);
        _scene.setIsOverTrue();
    }


}
