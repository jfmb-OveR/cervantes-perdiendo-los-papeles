﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(MoveToDestination))]
public class SanchoScript : MonoBehaviour {
    [Header("Manager para finalizar la escena")]
    public SceneManager _sceneManager;
    [Header("En el propio objeto y sirve para mover el papel a Cervantes")]
    public MoveToDestination _paperMove;
    public GameObject goDestination;
    public float fSpeed;

    [Header("Manuscrito")]
    public GameObject goManuscrito;
    [Header("Tag Objeto: llave, pistollas...")]
    public string sTag;

    public AudioSource _sound;
    public AudioSource _soundError;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    public ParticleSystem _particleSystem;

    public SecundarioController _secundario;


    private LanguageManager _languageManager;
    private bool bReached = false;

    private Vector3 vDestino;

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag != "Untagged" && other.tag != "Manuscrito")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                if (other.tag == sTag)
                {
                    //            Debug.Log("Llave entregada");
                    //                    other.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    Destroy(other.gameObject);
                    _sound.Play();
                    _secundario.setContento();

                    vDestino = goDestination.transform.position;
                    _paperMove.setDestino(vDestino);
                    _paperMove.setMove(true);

                    StartCoroutine(Wait());
                }
                else
                {
                    if (bReached == false)
                    {
                        _soundError.Play();
                        bReached = true;
                        _sceneManager.IncrementErrors(1);
                        switch (Random.Range(1, 6))
                        {
                            case 1:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                                break;
                            case 2:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                                break;
                            case 3:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                                break;
                            case 4:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                                break;
                            case 5:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                                break;
                        }
                        iImagenError.enabled = true;
                        tTextoError.enabled = true;
                        StartCoroutine(ocultarError());
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag != "Untagged" && other.tag != "Manuscrito")
            other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Untagged" && other.tag != "Manuscrito")
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
                other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }
    // Use this for initialization

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(1);
        _sceneManager.setIsOverTrue();
    }

    void Awake()
    {
        _languageManager = LanguageManager.Instance;
        vDestino = goDestination.transform.position;
        _paperMove.setSpeed(fSpeed);
        _paperMove.setKeepObject(true);
        _paperMove.setMove(false);
    }
}
