﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(BoxCollider2D))]
public class cambiarObjeto : MonoBehaviour {
    [Header("Primer objecto en pantalla")]
    public GameObject objeto;
    //    public SpriteRenderer objetoRoto;
    [Header("Objecto que va a sustituir al primero")]
    public GameObject otroObjecto;
    public string sOtherTag;
    //    public SceneManager05 _scene;
    [Header("True si hay que cambiar el objecto del inventario al interactuar")]
    public bool bCambiarObjecto;
    public GameObject goObjetoInventario;
    public GameObject goNuevoObjectoInventario;

    [Header("Otros objetos que hay que activar")]
    public GameObject[] sNuevoObjetos;

    public AudioSource _soundSuccess;
    public AudioSource _soundError;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    public SceneManager _scene;

    public ParticleSystem _particleSystem;

    private LanguageManager _languageManager;
    private bool bReached = false;

    private bool bObjetoCambiado;

    //    private bool bMove;

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag != "Untagged")
        {
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {

                if (other.tag == sOtherTag)
                {
                    if (bReached == false)
                    {
                        objeto.GetComponent<SpriteRenderer>().enabled = false;
                        objeto.GetComponent<BoxCollider2D>().enabled = false;

                        otroObjecto.GetComponent<SpriteRenderer>().enabled = true;
                        otroObjecto.GetComponent<BoxCollider2D>().enabled = true;

                        bObjetoCambiado = true;

                        Destroy(other.gameObject);

                        if (bCambiarObjecto == true)
                        {
                            goObjetoInventario.GetComponent<SpriteRenderer>().sortingOrder = -1;
                            goObjetoInventario.GetComponent<SpriteRenderer>().enabled = false;
                            goObjetoInventario.GetComponent<BoxCollider2D>().enabled = false;

                            goObjetoInventario.SetActive(false);

                            goNuevoObjectoInventario.GetComponent<SpriteRenderer>().enabled = true;
                            goNuevoObjectoInventario.GetComponent<BoxCollider2D>().enabled = true;
                            _particleSystem.transform.position = goNuevoObjectoInventario.transform.position;
                            _particleSystem.Play();
                        }
                    }

                    if (sNuevoObjetos.Length > 0)
                    {
                        for (int i = 0; i < sNuevoObjetos.Length; i++)
                        {
                            sNuevoObjetos[i].SetActive(true);
                            //                    sNuevoObjetos[i].GetComponent<BoxCollider2D>().enabled = true;
                        }
                    }
                    _soundSuccess.Play();
                    //            bMove = true;
                }
                else
                {
                    if (bReached == false)
                    {
                        _soundError.Play();
                        bReached = true;
                        _scene.IncrementErrors(1);
                        switch (Random.Range(1, 6))
                        {
                            case 1:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                                break;
                            case 2:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                                break;
                            case 3:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                                break;
                            case 4:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                                break;
                            case 5:
                                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                                break;
                        }
                        iImagenError.enabled = true;
                        tTextoError.enabled = true;
                        StartCoroutine(ocultarError());
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag != "Untagged")
            other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag != "Untagged")
            if(other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
                other.gameObject.GetComponent<DragDropScript>().setIsDropped(false);
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    void OnMouseDown()
    {
        _soundError.Play();
    }


    public bool getObjetoCambiado()
    {
        return (bObjetoCambiado);
    }

    void Awake()
    {
        _languageManager = LanguageManager.Instance;

        otroObjecto.GetComponent<SpriteRenderer>().enabled = false;
        otroObjecto.GetComponent<BoxCollider2D>().enabled = false;
        if (bCambiarObjecto == true)
        {

            goNuevoObjectoInventario.GetComponent<SpriteRenderer>().enabled = false;
            goNuevoObjectoInventario.GetComponent<BoxCollider2D>().enabled = false;
        }

        for (int i = 0; i < sNuevoObjetos.Length; i++)
        {
            sNuevoObjetos[i].SetActive(false);
            //                    sNuevoObjetos[i].GetComponent<BoxCollider2D>().enabled = true;
        }
    }
}

