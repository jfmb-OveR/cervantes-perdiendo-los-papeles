﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pistasScript : MonoBehaviour {
    public Image _imageAviso;
    public Text _textoAviso;
    public Text _botonAceptarAviso;
    public Text _botonCerrarAviso;

    public Image _imagePistas;
    public Text _titulo;
    public Text _texto;
    public Text _botonCerrar;
//    public UnityAds _anuncio;

    [Header("Canvas Objetivos")]
    public Image _imageObjetivos;
    public Text _textoObjetivos;
    public Text _botonCerrarObjetivos;

    private bool bPistasEnabled = false;

    public void mostrarPistas()
    {
        bPistasEnabled = true;
//        _anuncio.ShowAd();

        ocultarAviso();

        Time.timeScale = 0;
        _imagePistas.enabled = true;
        _titulo.enabled = true;
        _texto.enabled = true;
        _botonCerrar.enabled = true;
    }

    public void ocultarPistas()
    {
        bPistasEnabled = false;
        _imagePistas.enabled = false;
        _titulo.enabled = false;
        _texto.enabled = false;
        _botonCerrar.enabled = false;
        Time.timeScale = 1;
    }

    public void mostrarAviso()
    {
        if (bPistasEnabled == false)
        {
            _imageAviso.enabled = true;
            _textoAviso.enabled = true;
            _botonAceptarAviso.enabled = true;
            _botonCerrarAviso.enabled = true;
            Time.timeScale = 0;
        }
    }
    public void ocultarAviso()
    {
        _imageAviso.enabled = false;
        _textoAviso.enabled = false;
        _botonAceptarAviso.enabled = false;
        _botonCerrarAviso.enabled = false;
        Time.timeScale = 1;
    }

    public void ocultarObjetivos()
    {
        _imageObjetivos.enabled = false;
        _textoObjetivos.enabled = false;
        _botonCerrarObjetivos.enabled = false;
    }


    public void mostrarObjetivos()
    {
        _imageObjetivos.enabled = !_imageObjetivos.enabled;
        _textoObjetivos.enabled = !_textoObjetivos.enabled;
        _botonCerrarObjetivos.enabled = !_botonCerrarObjetivos.enabled;
    }
}
