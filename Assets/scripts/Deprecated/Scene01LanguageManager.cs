﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class Scene01LanguageManager : MonoBehaviour {
    public Text tBotonPistas;
    public Text tBotonCerrarDialogos;

    public Text tObjetivos;

    public Text tAceptarPistas;
    public Text tBotonAceptarAvisoPistas;
    public Text tBotonCerrarAvisoPistas;

    public Text tTituloMostrarPistas;
    public Text tMostrarPistas;
    public Text tButtonCerrarPistas;
//    public Text tObjetivos;

    public Text tEsc;
    public Text tBotonSi;
    public Text tBotonNo;

    private LanguageManager _languageManager;
    // Use this for initialization
    void Start()
    {
        _languageManager = LanguageManager.Instance;

        tBotonPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Pistas");
        tBotonCerrarDialogos.text = _languageManager.GetTextValue("Auxiliar.Boton.Cerrar");
        tAceptarPistas.text = _languageManager.GetTextValue("Auxiliar.Aviso.Pistas.Texto");

        tBotonAceptarAvisoPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Aceptar");
        tBotonCerrarAvisoPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Cerrar");

        tTituloMostrarPistas.text = _languageManager.GetTextValue("Auxiliar.Mostrar.Pistas.Titulo");
        tMostrarPistas.text = _languageManager.GetTextValue("Auxiliar.Mostrar.Pistas.Texto");
        tButtonCerrarPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Cerrar");
      
        tBotonPistas.text = _languageManager.GetTextValue("Auxiliar.Boton.Pistas");
        tBotonCerrarDialogos.text = _languageManager.GetTextValue("Auxiliar.Boton.Cerrar");
        tObjetivos.text = _languageManager.GetTextValue("Auxiliar.Texto.Objetivos");

        tEsc.text = _languageManager.GetTextValue("Auxiliar.Esc.Text");
        tBotonSi.text = _languageManager.GetTextValue("Auxiliar.Esc.Si");
        tBotonNo.text = _languageManager.GetTextValue("Auxiliar.Esc.No");
    }
}
