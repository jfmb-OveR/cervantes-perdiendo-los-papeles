﻿using UnityEngine;
using System.Collections;
using SmartLocalization;
using UnityEngine.UI;

public class CutScenesLanguageManager : MonoBehaviour {

    public Text tTexto;

    private LanguageManager _languageManager;
    // Use this for initialization
    void Start()
    {
        _languageManager = LanguageManager.Instance;

        tTexto.text = _languageManager.GetTextValue("Auxiliar.Click.Screen");
    }
}
