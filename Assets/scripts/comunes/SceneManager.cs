﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneManager : MonoBehaviour
{
    [Header("Canvas")]
    public Canvas _menu;
    public Image _imagenObjectivos;
    public Text _textoObjetivos;
    public Text _textoBotonObjetivos;
    public Text _tBotonCerrarObjetivos;
    public Canvas _canvasDialogos;
    public Image _imagenError;
    public Text _tTextoError;

    [Tooltip("Botón para cerrar el diálogo")]
    public Text _botonCerrar;
    public Text _botonPistas;

    [Header("Cámaras")]
    public Camera _firstCamera;
    public Camera _secondCamera;

    //    public CharacterController _controller;
    [Header("Managers")]
    public CharacterManagerScene01 _characterManager;
    public ObjectsManager _objectsManager;
//    public SpriteRenderer _objetosInventario; //Objeto que se debe poner Enabled en el inventario
//    public ParticleSystem _particulas; 
//    public SpriteRenderer[] aImagenes;

//    [Header("Objetos necesarios para acabar")]
//    public int iTotalObjetos;
    [Header("Siguiente escena")]
    public int iNewScene;
    [Header("PlayerPref")]
    public string PlayerPrefName;

    [Header("Array con los objetos del inventario")]
    public GameObject[] arrayObjetosInventario;
    //    public GameObject[] goObjetos;

    [Header("Texto de carga")]
    public Text textLoading;

    private bool bCollidersActivated;
    private bool bObjetivosVistos;

    private bool bIsOver;

    private int iNumErrors;

    private bool bLoadScene = false;

//   private int iObjects = 0;
/*
    public void IncrementObject()
    {
        iObjects++;
        Debug.Log("Número de objetos: " + iObjects);
    }
*/
    #region getters
/*
    public int getNumberOfObjects()
    {
        return iObjects;
    }
    */
    public bool getIsOver()
    {
        return bIsOver;
    }

    public int getNumErrores()
    {
        return iNumErrors;
    }

    #endregion

    #region setters
    public void setIsOverTrue()
    {
        bIsOver = true;
    }

    public void setCameraEnabled(bool enabled)
    {
        _botonPistas.enabled = !enabled;
        _botonCerrar.enabled = enabled;
        _canvasDialogos.enabled = enabled;
        //        if(_secondCamera != null)
        _firstCamera.enabled = !enabled;
        _secondCamera.enabled = enabled;

        if (enabled == false)
        {
            _characterManager.setEnded(true);
        }
    }
    #endregion

    public void IncrementErrors(int i)
    {
        iNumErrors += i;
    }

    void Awake()
    {
        _firstCamera.enabled = true;
        _secondCamera.enabled = false;

        _imagenObjectivos.enabled = false;
        _textoObjetivos.enabled = false;
        _tBotonCerrarObjetivos.enabled = false;
        _textoBotonObjetivos.enabled = false;

        _imagenError.enabled = false;
        _tTextoError.enabled = false;

        _botonCerrar.enabled = false;
        _botonPistas.enabled = false;

        _objectsManager.desactivarColliders(); //Desactivamos los objetos para que no se pulsen mientras Cervantes camina

        textLoading.enabled = false;

        bObjetivosVistos = false;

        //Desactivamos los objetos del inventario
        for (int i=0; i < arrayObjetosInventario.Length; i++)
        {
            arrayObjetosInventario[i].GetComponent<Collider2D>().enabled = false;
            arrayObjetosInventario[i].GetComponent<SpriteRenderer>().enabled = false;
        }
        bCollidersActivated = _objectsManager.getCollidersActivated();
    }

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1;
        bIsOver = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
        }


        if (Input.GetKeyDown(KeyCode.Escape)) //Esto se activa cuando el jugador le da a Escape. Aparece el menú de pausa
        {
            if (Time.timeScale == 1)
            {
                Stop();
            }
            else
            {
                Continue();
            }
        }

        if (_characterManager.getEnded() == true) //Comprobamos que los personajes han termiando de hablar
        {
            if(_objectsManager.getCollidersActivated() == false)
            {
                if(_secondCamera != null)
                {
                    setCameraEnabled(false);
//                    Debug.Log("Los personajes han terminado de hablar");
                }

                _objectsManager.activarColliders();//Activamos los colliders cuando Cervantes ya ha terminado

                if(bObjetivosVistos == false)
                {
                    bObjetivosVistos = true;

                    _botonPistas.enabled = true;
                    _textoBotonObjetivos.enabled = true;
                    _imagenObjectivos.enabled = true;
                    _textoObjetivos.enabled = true;
                    _tBotonCerrarObjetivos.enabled = true;
                    StartCoroutine(ocultarObjetivos());
                }
            }
            

            if (bIsOver == false)
            {
//            if (iObjects >= iTotalObjetos)
//                if (_objectsManager.getNumberOfObjects() >= iTotalObjetos)
/*                if(_objectsManager.getObjectsEnded() == true)
                { 
                    if ((_objectsManager.getNumberOfObjects() == iTotalObjetos) || (_objectsManager.getNumberOfObjects() == iTotalObjetos + 1))
                    {
                        _objetosInventario.enabled = true;
                        _particulas.Play();
                        //                        IncrementObject();
                        _objectsManager.IncrementObject();
                        StartCoroutine(DesactivarObjetos());
                    }
                }
                */
            }
            else
            {
                //TO DO: hacer una pantalla de resultados antes de cargar la escena nueva
                LoadNewLevel(iNewScene, true);
                bIsOver = false;
            }
        }
    }
/*
    IEnumerator DesactivarObjetos()
    {
        yield return new WaitForSeconds(0.3f);
        _objectsManager.desactivarColliders();
        _objectsManager.desactivarSprites();
    }
*/
    IEnumerator ocultarObjetivos()
    {
        yield return new WaitForSeconds(10);
        _imagenObjectivos.enabled = false;
        _textoObjetivos.enabled = false;
        _tBotonCerrarObjetivos.enabled = false;
    }

    /// <summary>
    /// Menu options
    /// </summary>
#region Funciones Menu   
    public void GoMenu()
    {
        LoadNewLevel(0, false);
//        Application.LoadLevel(0);
    }

    public void Stop()
    {
        _menu.enabled = true;
        Time.timeScale = 0;
    }

    public void Continue()
    {
        _menu.enabled = false;
        Time.timeScale = 1;
    }

    #endregion

    public void LoadNewLevel(int i, bool finnished)
    {
        if (finnished == true)
            myGameManager.myGameInstance.updateSceneStatus(PlayerPrefName, 1);
//            PlayerPrefs.SetInt(PlayerPrefName,1);

        bLoadScene = true;
        StartCoroutine(LoadScene(i));
    }

    IEnumerator LoadScene(int newScene)
    {
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }
}