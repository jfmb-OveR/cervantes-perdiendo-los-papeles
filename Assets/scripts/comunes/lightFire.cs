﻿using UnityEngine;
using System.Collections;

public class lightFire : MonoBehaviour {
    public float fMaxValue = 8f;
    public float fMinValue= 6f;

//    public float fSpeed=60f;

    private Light myLight;
//    public GameObject goLight;

    void Awake()
    {
        myLight = this.GetComponent<Light>();
    }
	
	// Update is called once per frame
	void Update () {
        myLight.intensity = Random.Range(fMinValue, fMaxValue+1);
//        Debug.Log("Intensidad: " + myLight.intensity);
	}
}
