﻿using UnityEngine;
using System.Collections;

public class CharacterController : MonoBehaviour {

    public enum State
    {
        IDLE,
        WALKING,
    }

    public State state;
    public Animator anim;
    public SpriteRenderer characterSprite;

    private bool bIsWalking;
    private int walkingHash = Animator.StringToHash("isWalking");

    private bool bIsBlocked;

    private Vector3 vStart;
    private Vector3 vDestination;
    private float fSpeed;

    private Quaternion _quaternion = new Quaternion(0,0,0,0);
//    public GameObject _objeto;

    #region getters
    public bool getIsBlocked()
    {
        return bIsBlocked;
    }
    #endregion

    #region setters
    public void setIsWalking(bool newValue)
    {
        bIsWalking = newValue;
    }

    public void setStateIDLE()
    {
        state = State.IDLE;
    }

    public void setStateWALKING()
    {
        state = State.WALKING;
    }

    public void setBlocked(bool newValue)
    {
        bIsBlocked = newValue;
    }

    public void setStart(Vector3 newStart)
    {
        vStart = newStart;
    }

    public void setDestination(Vector3 newDest)
    {
        vDestination = newDest;
    }

    public void setSpeed(float newSpeed)
    {
        fSpeed = newSpeed;
    }
    #endregion

    void Awake()
    {
        state = State.IDLE;
    }

	// Use this for initialization
	void Start () {
        bIsWalking = false;
        bIsBlocked = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (!bIsBlocked)
        {
            switch (state)
            {
                case State.IDLE:
                    bIsWalking = false;
                    anim.SetBool(walkingHash, bIsWalking);
//                    Debug.Log("ESTADO IDLE");
                    break;
                case State.WALKING:
                    bIsWalking = true;
                    anim.SetBool(walkingHash, bIsWalking);
                    characterMoving();
//                    Debug.Log("ESTADO WALKING   ");
                    break;
            }
        }
    }



    //    public void characterMoving(Vector3 inicio, Vector3 destino, float velocidad)
    public void characterMoving()
    {
        //        this.transform.position = Vector3.MoveTowards(inicio, destino, velocidad);
        this.transform.position = Vector3.MoveTowards(vStart, vDestination, fSpeed * Time.deltaTime);
    }

    public void characterFlipX()
    {
        characterSprite.flipX = !characterSprite.flipX;
    }

}
