﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveToDestination))]
public class PointClickChangingImage: MonoBehaviour
{
    public GameObject goObjetoInventario; //Es el destino al que se dirigirá el objeto
    public ParticleSystem _efectoParticulas;

    public ObjectsManager _objectsManager;
    public AudioSource _sound;
    public bool bConservar; //Con esta variable especificamos si queremos conservar el sprite en el inventario o no

    public SpriteRenderer imagenInicial;
    public SpriteRenderer imagenFinal;

    public float fSpeed;
    public MoveToDestination _movementController;

    private SpriteRenderer Inventario;
    private Vector3 vDestino;

    private bool bUsed;
    private bool bObjetoInvetarioActivado = false;

    //    private bool bMove = false;

    void Start()
    {
//        goObjetoInventario.GetComponent<SpriteRenderer>().enabled = false;
//        goObjetoInventario.GetComponent<BoxCollider2D>().enabled = false;

        Inventario = goObjetoInventario.GetComponent<SpriteRenderer>();
        imagenFinal.enabled = false;
        vDestino = goObjetoInventario.transform.position;
        bUsed = false;
    }

    void Update()
    {
        if (_movementController.getArrived() == true)
        {
            if (bConservar == false && bObjetoInvetarioActivado == false)
            {
                this.GetComponent<SpriteRenderer>().enabled = false;
                Inventario.enabled = true;
                goObjetoInventario.GetComponent<BoxCollider2D>().enabled = true;
                bObjetoInvetarioActivado = true;
                _efectoParticulas.transform.position = goObjetoInventario.transform.position;
                _efectoParticulas.Play();
            }
        }
    }

    void OnMouseDown()
    {
        if(bUsed == false)
        {
            _movementController.setDestino(vDestino);
            _movementController.setSpeed(fSpeed);
            _movementController.setMove(true);

            //        bMove = true;
            _objectsManager.IncrementObject();
            _sound.Play();

            imagenInicial.enabled = false;
            imagenFinal.enabled = true;

            bUsed = true;
        }
    }
}

