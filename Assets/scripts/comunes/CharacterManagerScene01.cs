﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SmartLocalization;
using UnityEngine.SceneManagement;

public class CharacterManagerScene01 : MonoBehaviour
{
    public float fSpeed;
    public GameObject goDestino;
    public CharacterController character;
    public SceneManager _scene;

    public bool bHayDialogo = true;
    public Image imageCervantes;
    public Image imageSecundario;
    public Text textCervantes;
    public Text textSecundario;

    private Vector3 vDestino;
    private bool bArrived;
    private bool bDialogsEnded;
    private int iScene;

//    private bool bClick = false;
    private int iNumClicksDialogos = 0;

    private LanguageManager languageManager;

    #region getters
    public bool getEnded()
    {
        return bDialogsEnded;
    }
    #endregion

    #region setters
    public void setHayDialogo (bool bNewValue)
    {
        bHayDialogo = bNewValue;
    }

    public void setEnded(bool newValue)
    {
        bDialogsEnded = newValue;
    }

    public void setArrived(bool newValue)
    {
        bArrived = newValue;
    }

    public void setScene(int i)
    {
        iScene = i;
    }

    public void setDestination(Vector3 newDestination)
    {
        vDestino = newDestination;
    }

    public void setSpeed(float newSpeed)
    {
        fSpeed = newSpeed;
    }

    public void setCharacterMovingParameters()
    {
        character.setStart(character.transform.position);
        character.setDestination(vDestino);
        character.setSpeed(fSpeed);
    }
    #endregion
    // Use this for initialization

    void Awake()
    {
        imageCervantes.enabled = false;
        imageSecundario.enabled = false;

        setCharacterMovingParameters();
        bArrived = false;

        //        character.setStart(character.transform.position);
        //        character.setDestination(vDestino);
        //        character.setSpeed(fSpeed);
    }

    void Start()
    {
        languageManager = LanguageManager.Instance;
//        languageManager.ChangeLanguage("es");
        vDestino = goDestino.transform.position;
        bDialogsEnded = false;
//        character.setStateWALKING();

        character.setStart(character.transform.position);
        character.setDestination(vDestino);
        character.setSpeed(fSpeed);

        iScene = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
    }

    // Update is called once per frame
    void Update()
    {
        if (bArrived == false)
        {
            character.setStateWALKING();
            character.setStart(character.transform.position);
//            character.setDestination(vDestino);
//            character.setSpeed(fSpeed);
//            character.characterMoving(character.transform.position, vDestino, fSpeed * Time.deltaTime);
//            character.transform.position = Vector3.MoveTowards(character.transform.position, vDestino, fSpeed * Time.deltaTime);
            if (character.transform.position == vDestino) 
            {
                bArrived = true;
                character.setStateIDLE();
                if(bHayDialogo == true)
                {
                    _scene.setCameraEnabled(true);
                    imageCervantes.enabled = true;
                    imageSecundario.enabled = true;
                    iNumClicksDialogos++;
                }
            }
        }
        else
        {
            if (bDialogsEnded == false)
            {
//                Debug.Log("El diálogo no ha terminado");
                switch (iScene)
                {
                    case 4: //Escena 1: Galatea
                        startTalkingScene01();
                        //StartCoroutine(startTalkingScene01(4f));
                        break;
                    case 7:  //Escena 2.1: Vidriera
                        startTalkingScene02();
                        //                    StartCoroutine(startTalkingScene02(4f));
                        break;
                    case 8: //Escena 2.2: Vidriera
                        bDialogsEnded = true;
                        break;
                    case 9: //Escena 2.3: Vidriera
                        bDialogsEnded = true;
                        break;
                    case 12: //Escena 3: Quijote
                        startTalkingScene03();
                        //                    StartCoroutine(startTalkingScene03(4f));
                        break;
                    case 15: //Escena 4: Juan de Jauregui
                        startTalkingScene04();
                        //                    StartCoroutine(startTalkingScene04(4f));
                        break;
                    case 18: //Escena 5: Sancho
                        startTalkingScene05();
                        //                    StartCoroutine(startTalkingScene05(4f));
                        break;
                    case 21: //Escena 6: Cide Hamete y la Alhambra
                        startTalkingScene10();
                        //                    StartCoroutine(startTalkingScene10(4f));
                        break;
                    case 24: //Escena 7: Caballeros
                        startTalkingScene07();
                        //                    StartCoroutine(startTalkingScene07(4f));
                        break;
                    case 27: //Escena Barbero
                        startTalkingScene12();
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 30: //Escena Maritornes
                        startTalkingScene13();
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 33: //Escena Rinconete
                        startTalkingScene06();
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 34: //Escena Rinconete
                        bDialogsEnded = true;
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 35: //Escena Rinconete
                        bDialogsEnded = true;
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 38: //Escena Roque
                        startTalkingScene08();
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 41: //Escena Roque
                        startTalkingScene09();
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 44: //Escena Cipion exterior
                        startTalkingScene14();
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 45: //Escena Cipion hall
                        bDialogsEnded = true;
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 46: //Escena Cipion despacho
                        bDialogsEnded = true;
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 47: //Escena Cipion cocina
                        bDialogsEnded = true;
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 48: //Escena Cipion biblioteca
                        bDialogsEnded = true;
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                    case 49: //Escena Cipion biblioteca
                        bDialogsEnded = true;
                        //                    StartCoroutine(startTalkingScene12(4f));
                        break;
                }
            }
        }


    }

 
    public void characterFlipX()
    {
        character.characterFlipX();
    }

    void OnMouseDown()
    {

        iNumClicksDialogos++;
    }

    public void startTalkingScene01()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = ""; 
                break;
            case 1: //¿Qué te ocurre Galatea?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = ""; 
                break;
            case 2: //He perdido mi cayado.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Galatea.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 3: //Oh vaya. ¿Tu callado?
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Cervantes.Frase02");
                break;
            case 4: //¡No, mi CAYADO! Mi bastón para pastorear.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Galatea.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 5: //... para pastorear...
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Cervantes.Frase03");
                break;
            case 6: //Sí, para dirigir a mis ovejas. Se me cayó al río. Por favor, ayúdame a recuperarlo.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Galatea.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 7: //Además, con él podrás coger tu manuscrito, que está en ese árbol.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Galatea.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 8:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;

        }
    }

    public void startTalkingScene02()
    {
//        Debug.Log("Estoy en el diáologo y estos son los clicks: " + iNumClicksDialogos);
        switch (iNumClicksDialogos)
        {
            case 0: 
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //Hola Licenciado. Necesito recuperar mi manuscrito
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;

            case 2: //Está en esa celda. Supongo que algo habrá "echo". 
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;

            case 3: //Será "hecho"...
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase02");
                break;

            case 4: //...
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;

            case 5: //Bueno, ¿cómo puedo liberarlo?
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase03");
                break;

            case 6: //Si liberas a tu manuscrito me tienes que liberar a mí también.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;

            case 7: //Te diré dónde está la llave si prometes quitarme esta pesada bola.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase04");
                textCervantes.GetComponent<Text>().text = "";
                break;

            case 8:  //"¡Echo!"... Digo "¡hecho!"
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase04");
                break;

            case 9: //La llave está en esta oficina. Podrás entrar si rompes la puerta con algo contundente.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase05");
                textCervantes.GetComponent<Text>().text = "";
                break;

            case 10: //¡Gracias! Lo "aré"... "¡Haré!" grrrrrr
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase05");
                break;

            case 11:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;
        }
    }

    public void startTalkingScene03()
    {
        switch (iNumClicksDialogos)
        {
            case 0: 
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: // Hola Don Quijote, te veo quemado.
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = ""; textSecundario.GetComponent<Text>().text = "";
                break;
            case 2: //Lo estoy y quemado va a acabar este libro. ¡En él se cuentan mentiras sobre mí!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 3://Entiendo que no es buen momento para pedirte mi manuscrito, ¿verdad? 
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = ""; 
                break;
            case 4: //Ayúdame a quemar este libro apócrifo y te lo daré.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase02"); 
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 5: //... 
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = ""; 
                break;
            case 6: //¿No sabes qué significa apócrifo?
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 7: //No...
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = ""; 
                break;
            case 8: //¡Falso, mentiroso! ¡Eso significa!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase04");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 9: //¿Me devolverás mi manuscrituo si te ayudo a quemar ese libro aprofi... acrofi...?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase05");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 10: //¡Apócrifo!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase05");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 11: //¿Me devolverás mi manuscrituo si te ayudo a quemar ese libro?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase06");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 12: //Os ayudaré, por mi honor de caballero y si no, ¡que todos estos gigantes me coman!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase06");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 13: //Ah sí... estos molinos... digo gigantes. 
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase07");
                break;
            case 14:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;
        }
    }

    public void startTalkingScene04()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //Estúpido Góngora... y si no, Lope de Vega... será posible lo que dicen de mí...
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 2: //¿Qué te ocurre, Juan de Jáuregui? ¿Qué pintas aquí?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 3: //Está visto que pintar, no pinto nada. Lope de Vega y Góngora critican todos mis escritos.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 4: //4. Me refería a qué estabas pintando en tu taller.
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 5: //¡Es cierto, estaba pintando! Estoy tan enfadado que olvidé lo que estaba haciendo.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 6: //No encuentro mis útiles de pintura y el modelo se ha cansado de esperar.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase04");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 7: //7. Sí, tiene mala pinta. Bueno, dame mi manuscrito y te dejaré tranquilo.
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 8: //Hagamos un trato: consígueme un pincel y los colores blanco, negro y naranja y te daré tu manuscrito.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase05");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 9: //No parece difícil. ¿Eso es todo?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 10: //10. No, también necesitaré que poses para mí, para que te pinte.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase06");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 11: //¿Posar yo de modelo? ¡Ni en pintura!
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase05");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 12:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;
        }
    }

    public void startTalkingScene05()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //1.Será posible! ¡No me dejen entrar por no tener la llave de mi propia ciudad!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Sancho.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 2: //2.¿Qué cuentas Sancho? 
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 3: //3. - 1º: soy el gobernador -2º: el guardia debe obedecerme -3º: ¡la llave no me cabe en el bolsillo!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Sancho.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 4: //4. ¿Me podrías devolver mi manuscrito?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 5: //5. Ayúdame a abrir la puerta y te devolveré tu papel.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Sancho.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 6: //6. ¿Y cuál es la clave para abrir la puerta?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 7: //7. ¡La clave es la llave! ¡Y ese cerdo se la quiere comer!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Sancho.Frase04");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 8: //8. Interesante... quizá algo de comida nos dé la clave y así el cerdo nos dará la llave
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 9:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;

        }
    }

    public void startTalkingScene06()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //1. ¡Eh pilluelos! Devolvedme mi manuscrito.
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene10.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 2: //2. ¿Qué nos das a cambio?
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene10.Rinconete.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 3: //3. Os daré las gracias y no avisaré al alguacil.
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene10.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 4: //4. De eso ni hablar. Tenemos que volver a Sevilla con un botín para nuestro jefe Monipodio.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene10.Rinconete.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 5: //5. ¿Un botín? Necesitará un par para los dos pies, ¿no?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene10.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 6: //6. ¿Qué dices? ¡Un botín fruto de nuestros robos! Pertenecemos a una banda.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene10.Rinconete.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 7: //7. Lástima que no sea una banda de música...
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene10.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 8: //8.  Os traeré algo para Monipodio y me devolveréis mi manuscrito.
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene10.Cervantes.Frase05");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 9: //8. Tráenos 3 objetos y nos iremos con la música a otra parte.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene10.Rinconete.Frase04");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 10:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;

        }
    }

    public void startTalkingScene07()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //1. Espere un momento Señor Cervantes, ahora le atiendo. Estamos justo en una justa.
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 2: //2. ¡Devuélvame mi alfanje, malandrín!
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 3: //3. ¡Devuélvame mi lanza, ladrón!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 4: //4. ¡Yo no he robado nada y nadie me llama ladrón!
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 5: //5. ¡Mi lanza ha desaparecido y sólo le veo a usted aquí!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 6: //6. ¡Le desafío a un duelo! ¡Devuélvame mi alfanje, malandrín!
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 7: //7. ¡Encantado! ¡En cuanto me devuelva mi lanza, ladrón!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 8: //8. Frase Verde 3
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 9: //9. Frase Blanca 2
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 10://10. Frase Verde 4
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 11: //11. ¡Encantado! ¡En cuanto me devuelva mi lanza, ladrón!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 12:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;

        }
    }

    public void startTalkingScene08()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //1. Pobre perro, ¡está sediento!
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene08.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 2: //2. Sediento y cabreado. ¡Soy un pistolero sin pistolas!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene08.Roque.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 3: //3. ¿Si recupero tus pistolas me devuelves mi manuscrito?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene08.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 4: //4. Por supuesto pero ya me explicarás cómo vas a encontrar agua en esta galera abandonada...
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene08.Roque.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 5: //5. Quizá si consigo depurar el agua de ese cubo... pero ¿cómo?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene08.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 6: //5. Quizá si consiga depurar el agua de ese cubo... pero ¿cómo?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene08.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 7:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;

        }
    }

    public void startTalkingScene09()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //1. ¡Señor Cervantes, por aquí! ¡Ayúdeme!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene09.Pasamonte.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 2: //2. Dígame Don Ginés, ¿qué necesita? Un momento... ¿cómo sabía que venía?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene09.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 3: //3. Mi mono adivino me lo dijo. Es un genio, aunque algunos no crean sus predicciones.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene09.Pasamonte.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 4: //4. ¿En qué quedamos? ¿Su mono es adivino o genio?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene09.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 5: //5. ¡Lo que sea! Lo han apresado y lo envían a las galeras.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene09.Pasamonte.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 6: //5 .Déjame que adivine, si lo libero me devuelves mi manuscrito ¿verdad?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene09.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 7: //5. ¿Cóimo lo ha sabido? ¡Es un usted un genio!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene09.Pasamonte.Frase04");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 8: //5. Ay señor... dame paciencia, ¡la madre de la ciencia!
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene09.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 9:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;

        }
    }

    public void startTalkingScene10()
    {
        switch (iNumClicksDialogos)
        {
            case 0: 
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //1. Hola Cide Hamete, ¿tú también estas turbado?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 2: //2. ¡Estoy hasta el turbante! ¡Felinos, me arruinarán la vida!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 3: //3. No sé cómo ha acabado mi poema debajo de un león de piedra y no lo puedo coger.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 4: //4. Quizá podríamos hacer palanca con algo y moverlo.
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break; 
            case 5: //5. Lo más largo que veo es ese asta de ciervo, enganchada al árbol pero el gato no me deja acercarme.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 6: //6. El poema bajo el león, un asta de ciervo en el árbol y mi manuscrito ha llegado "asta" aquí ¿Qué ocurre en este lugar?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 7: //7. No preguntes... y es "hasta", no "asta".
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase04");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 8: //8. Te ayudaré con tu poema y tu me devuelves mi manuscrito. A ver cómo cojo el "hasta" de ciervo
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 9: //9. ¡"Asta", no "hasta"!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase05");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 10: //10. Pobre hombre, creo que vivir en la Alhambra le ha vuelto loco...
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase05");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 11:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;

        }
    }

    public void startTalkingScene12()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //1. Esto sí que no lo esperaba. ¿Un león en una barbería?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 2: //2. ¿Acaso necesita un arreglo en su melena?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 3: //3. ¿Qué melena ni qué melena? Hace unos días pasó un circo por aquí...
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Barbero.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 4: //4.El domador me preguntó si podía vigilar al león un rato ¡y de eso hace una semana!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Barbero.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 5: //5. Y de alguna forma mi manuscrito ha acabado en la jaula... 
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 6: //6.¡Ni se le ocurra abrir la jaula! O me da un arma y un yelmo o esa jaula no se abre.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Barbero.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 7: //7. ¿Un arma y un yelmo en una barbería? ¡Ni que esto fuera una armería!
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 8:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;
        }
    }

    public void startTalkingScene13()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //1. Ay... ay... ay... todo me duele y la bebida me tiene tiesa...
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Maritornes.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 2: //2. Hola Maritornes, necesito ese manuscrito que tienes en la mesa.
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 3: //3. Y yo necesito que se me vayan todos mis males. ¿Me puedes ayudar?
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Maritornes.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 4: //4. No soy médico pero algo se me ocurrirá...
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 5: //5. ¿Por qué no hablas con ese charlatán, Fieragrás? Parece que su bálsamo todo lo cura.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Maritornes.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 6: //6. Es caro ese remedio, va a ser una misión dura. 
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 7: //7. Tráeme el bálsamo y tu manuscrito te devolveré.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Maritornes.Frase04");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 8: //8. Sabía que algo querrías, era de preveer...
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 9: //9. ¿Cuándo ha empezado a hablar en verso vuestra merced?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Cervantes.Frase05");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 10: //10. Yo no soy la que rima, ha empezado usted.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Maritornes.Frase05");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 11: //11. ¡Basta ya, pardiez!
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene13.Cervantes.Frase06");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 12:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;
        }
    }

    public void startTalkingScene14()
    {
        switch (iNumClicksDialogos)
        {
            case 0:
                textCervantes.GetComponent<Text>().text = "";
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 1: //1. "La última fiesta antes de los exámenes" y de eso hace dos días.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cipion.Frase01");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 2: //2. Le cuentas tu vida a todo el mundo, Berganza y pasa lo que pasa.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cipion.Frase02");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 3: //3. Dos perros que hablan... ¿Será lo más raro que he visto en mi viaje?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cervantes.Frase01");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 4: //4. Perros y estudiantes de medicina. Se ha jubilado el alguacil y el nuevo no nos deja entrar. 
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cipion.Frase03");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 5: //5. Tenemos que estudiar, ¡empiezan los exámenes y ya hemos perdido mucho tiempo!
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cipion.Frase04");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 6: //6. No me digas más: el nuevo alguacil no se cree que dos perros parlantes sean también estudiantes, ¿verdad?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cervantes.Frase02");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 7: //7. ¡Exacto! ¿Te lo puedes creer?
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cipion.Frase05");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 8: //8.  No, no me lo creo. Literalmente. Oye, eso de ahí no será uno de mis manuscritos perdidos, ¿no?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cervantes.Frase03");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 9: //9. Sí, puede ser. Algo teníamos que leer mientras esperamos aquí.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cipion.Frase06");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 10: //10.Vale, yo os traigo vuestros libros de medicina y me devolvéis el manuscrito. ¿Trato hecho?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cervantes.Frase04");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 11: //11. Y nos traes algo de comer.
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cipion.Frase07");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 12: //12. Y os traigo algo de comer. ¿Vale?
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cervantes.Frase05");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 13: //13. Y te quedas un rato para escuchar nuestras historias..
                textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cipion.Frase08");
                textCervantes.GetComponent<Text>().text = "";
                break;
            case 14: //14. Aaaaay... y me quedo un rato para escuchar tus historias... Tal vez escriba algo con ellas...
                textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene11.Cervantes.Frase06");
                textSecundario.GetComponent<Text>().text = "";
                break;
            case 15:
                textSecundario.GetComponent<Text>().text = "";
                textCervantes.GetComponent<Text>().text = "";
                bDialogsEnded = true;
                break;
        }
    }

    /*
        IEnumerator startTalkingScene01(float segundos)
        {
            yield return new WaitForSeconds(0.5f);
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Cervantes.Frase01");
            yield return new WaitForSeconds(4);

            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Galatea.Frase01");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Cervantes.Frase02");
            yield return new WaitForSeconds(4);

            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Galatea.Frase02");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Cervantes.Frase03");
            yield return new WaitForSeconds(4);

            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Galatea.Frase03");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene01.Galatea.Frase03");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = "";
            bDialogsEnded = true;

            yield return null;
        }


        IEnumerator startTalkingScene02(float segundos)
        {
            yield return new WaitForSeconds(0.5f);

            //Hola Licenciado. Necesito recuperar mi manuscrito
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase01");
            yield return new WaitForSeconds(4);

            //Está en esa celda. Supongo que algo habrá "echo".
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase01");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //Será "hecho"...
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase02");
            yield return new WaitForSeconds(4);

            //...
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase02");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //Bueno, ¿cómo puedo liberarlo?
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase03");
            yield return new WaitForSeconds(4);

            //Si liberas a tu manuscrito me tienes que liberar a mí también.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase03");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //Te diré dónde está la llave si prometes quitarme esta pesada bola.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase04");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //"¡Echo!"... Digo "¡hecho!"
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase04");
            yield return new WaitForSeconds(4);

            //La llave está en esta oficina. Podrás entrar si rompes la puerta con algo contundente.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Vidriera.Frase05");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //¡Gracias! Lo "aré"... "¡Haré!" grrrrrr
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene02.Cervantes.Frase05");
            yield return new WaitForSeconds(4);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = "";

            bDialogsEnded = true;
        }

        IEnumerator startTalkingScene03(float segundos)
        {
            yield return new WaitForSeconds(0.5f);

            //        Hola Don Quijote, te veo quemado.
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase01");
            yield return new WaitForSeconds(3);

            //Lo estoy y quemado va a acabar este libro. ¡En él se cuentan mentiras sobre mí!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase01");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //Entiendo que no es buen momento para pedirte mi manuscrito, ¿verdad?
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase02");
            yield return new WaitForSeconds(4);

            //Ayúdame a quemar este libro apócrifo y te lo daré.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase02");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //...
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase03");
            yield return new WaitForSeconds(4);

            //¿No sabes qué significa apócrifo?
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase03");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //No...
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase04");
            yield return new WaitForSeconds(3);

            //¡Falso, mentiroso! ¡Eso significa!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase04");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //¿Me devolverás mi manuscrituo si te ayudo a quemar ese libro aprofi... acrofi...?
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase05");
            yield return new WaitForSeconds(3);

            //¡Apócrifo!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase05");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //¿Me devolverás mi manuscrituo si te ayudo a quemar ese libro?
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase06");
            yield return new WaitForSeconds(3);

            //Os ayudaré, por mi honor de caballero y si no, ¡que todos estos gigantes me coman!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Quijote.Frase06");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //Ah sí... estos molinos... digo gigantes.
            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene03.Cervantes.Frase07");
            yield return new WaitForSeconds(3);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = "";

            bDialogsEnded = true;
        }

        IEnumerator startTalkingScene04(float segundos)
        {
            yield return new WaitForSeconds(0.5f);

            //Estúpido Góngora... y si no, Lope de Vega... será posible lo que dicen de mí...
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase01");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //¿Qué te ocurre, Juan de Jáuregui? ¿Qué pintas aquí?
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase01");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //Está visto que pintar, no pinto nada. Lope de Vega y Góngora critican todos mis escritos.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase02");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //4. Me refería a qué estabas pintando en tu taller.
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase02");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //¡Es cierto, estaba pintando! Estoy tan enfadado que olvidé lo que estaba haciendo.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase03");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //No encuentro mis útiles de pintura y el modelo se ha cansado de esperar.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase04");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //7. Sí, tiene mala pinta. Bueno, dame mi manuscrito y te dejaré tranquilo.
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase03");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //Hagamos un trato: consígueme un pincel y los colores blanco, negro y naranja y te daré tu manuscrito.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase05");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //No parece difícil. ¿Eso es todo?
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase04");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //10. No, también necesitaré que poses para mí, para que te pinte.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Jauregui.Frase06");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //¿Posar yo de modelo? ¡Ni en pintura!
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene04.Cervantes.Frase05");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = "";

            bDialogsEnded = true;
        }
        IEnumerator startTalkingScene05(float segundos)
        {
            yield return new WaitForSeconds(0.5f);

            //1.Será posible! ¡No me dejen entrar por no tener la llave de mi propia ciudad!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Sancho.Frase01");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //2.¿Qué cuentas Sancho? 
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Cervantes.Frase01");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //3. - 1º: soy el gobernador -2º: el guardia debe obedecerme -3º: ¡la llave no me cabe en el bolsillo!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Sancho.Frase02");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(6);

            //4. ¿Me podrías devolver mi manuscrito?
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Cervantes.Frase02");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //5. Ayúdame a abrir la puerta y te devolveré tu papel.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Sancho.Frase03");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //6. ¿Y cuál es la clave para abrir la puerta?
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Cervantes.Frase03");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //7. ¡La clave es la llave! ¡Y ese cerdo se la quiere comer!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Sancho.Frase04");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //8. Interesante... quizá algo de comida nos dé la clave y así el cerdo nos dará la llave
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene05.Cervantes.Frase04");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(6);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = "";

            bDialogsEnded = true;
        }

        IEnumerator startTalkingScene07(float segundos)
        {
            yield return new WaitForSeconds(0.5f);

            //1. Espere un momento Señor Cervantes, ahora le atiendo. Estamos justo en una justa.
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase01");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //2. ¡Devuélvame mi alfanje, malandrín!
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase02");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //3. ¡Devuélvame mi lanza, ladrón!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase01");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //4. ¡Yo no he robado nada y nadie me llama ladrón!
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase03");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //5. ¡Mi lanza ha desaparecido y sólo le veo a usted aquí!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase02");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //6. ¡Le desafío a un duelo! ¡Devuélvame mi alfanje, malandrín!
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase04");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //7. ¡Encantado! ¡En cuanto me devuelva mi lanza, ladrón!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase03");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //8. Frase Verde 3
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase03");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //9. Frase Blanca 2
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase02");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //10. Frase Verde 4
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Verde.Frase04");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //11. ¡Encantado! ¡En cuanto me devuelva mi lanza, ladrón!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene07.Blanca.Frase01");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = "";

            bDialogsEnded = true;
        }

        IEnumerator startTalkingScene10(float segundos)
        {
            yield return new WaitForSeconds(0.5f);

            //1. Hola Cide Hamete, ¿tú también estas turbado?
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase01");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //2. ¡Estoy hasta el turbante! ¡Felinos, me arruinarán la vida!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase01");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //3. No sé cómo ha acabado mi poema debajo de un león de piedra y no lo puedo coger.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase02");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(6);

            //4. Quizá podríamos hacer palanca con algo y moverlo.
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase02");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //5. Lo más largo que veo es ese asta de ciervo, enganchada al árbol pero el gato no me deja acercarme.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase03");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(6);

            //6. El poema bajo el león, un asta de ciervo en el árbol y mi manuscrito ha llegado "asta" aquí ¿Qué ocurre en este lugar?
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase03");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(6);

            //7. No preguntes... y es "hasta", no "asta".
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase04");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //8. Te ayudaré con tu poema y tu me devuelves mi manuscrito. A ver cómo cojo el "hasta" de ciervo
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase04");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(6);

            //9. ¡"Asta", no "hasta"!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cide.Frase05");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //10. Pobre hombre, creo que vivir en la Alhambra le ha vuelto loco...
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene06.Cervantes.Frase05");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(6);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = "";

            bDialogsEnded = true;
        }

        IEnumerator startTalkingScene12(float segundos)
        {
            yield return new WaitForSeconds(0.5f);

            //1. Esto sí que no lo esperaba. ¿Un león en una barbería?
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Cervantes.Frase01");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //2. ¿Acaso necesita un arreglo en su melena?
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Cervantes.Frase02");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //3. ¿Qué melena ni qué melena? Hace unos días pasó un circo por aquí...
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Barbero.Frase01");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //4.El domador me preguntó si podía vigilar al león un rato ¡y de eso hace una semana!
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Barbero.Frase02");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(4);

            //5. Y de alguna forma mi manuscrito ha acabado en la jaula... 
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Cervantes.Frase03");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //6.¡Ni se le ocurra abrir la jaula! O me da un arma y un yelmo o esa jaula no se abre.
            textSecundario.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Barbero.Frase03");
            textCervantes.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            //7. ¿Un arma y un yelmo en una barbería? ¡Ni que esto fuera una armería!
            textCervantes.GetComponent<Text>().text = languageManager.GetTextValue("Scene12.Cervantes.Frase04");
            textSecundario.GetComponent<Text>().text = "";
            yield return new WaitForSeconds(5);

            textSecundario.GetComponent<Text>().text = "";
            textCervantes.GetComponent<Text>().text = "";

            bDialogsEnded = true;
        }
    */
}
