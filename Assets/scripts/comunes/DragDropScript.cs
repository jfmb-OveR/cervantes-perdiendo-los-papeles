﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class DragDropScript : MonoBehaviour {
//    public CharacterController _controller;
    public GameObject goObjetoInventario;
    public float fSpeed;

    private SpriteRenderer Inventario;
    private Vector3 vDestino;
//    private bool bIsEnabled = true;
    private bool bMove;
    private bool bUsed;
    private bool bIsDropped = false;

    Vector3 dist;
	float posX;
	float posY;

	float newPosX;
	float newPosY;

	Vector3 curPos = new Vector3();
	Vector3 worldPos = new Vector3();


    public bool getIsDropped()
    {
        return bIsDropped;
    }

    public void setIsDropped(bool newValue)
    {
        bIsDropped = newValue;
    }

    public void setMoved(bool newValue)
    {
        bMove = newValue;
    }

    public void setUsed(bool newValue)
    {
        bUsed = newValue;
    }
/*
    public void setEnabledFalse()
    {
        bIsEnabled = false;
    }
*/
    public void setNewDest(Vector3 newDest)
    {
        vDestino = newDest;
    }

	void OnMouseDown(){
		dist = Camera.main.WorldToScreenPoint (transform.position);
		posX = Input.mousePosition.x - dist.x;
		posY = Input.mousePosition.y - dist.y;
//        _controller.setBlocked(true);

        bIsDropped = false;
	}

	void OnMouseDrag(){
		curPos = new Vector3 (Input.mousePosition.x - posX, Input.mousePosition.y - posY, dist.z);
		worldPos = Camera.main.ScreenToWorldPoint (curPos);
		transform.position = worldPos;
        bMove = false;
    }

    void OnMouseUp()
    {
        bIsDropped = true;
        if (bUsed == false)
        {
//            _controller.setBlocked(false);
            bMove = true;
        }
    }

    void onTriggerEnter2D(Collider other)
    {
        Debug.Log("He entrado en colision");
    }


    void Start()
    {
        Inventario = goObjetoInventario.GetComponent<SpriteRenderer>();
        vDestino = goObjetoInventario.transform.position;
        bUsed = false;
    }

    void Update()
    {
        if (bMove == true)
        {
//            bIsDropped = false;
            this.transform.position = Vector3.MoveTowards(transform.position, vDestino, fSpeed * Time.deltaTime);
            if (transform.position == vDestino)
            {
                bMove = false;
                Inventario.enabled = true;
                //                this.GetComponent<SpriteRenderer>().enabled = false;
                //                this.GetComponent<Collider2D>().enabled = false;
            }
        }
    }
}
