﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MoveToDestination))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]

public class PointClick : MonoBehaviour {

    public GameObject goObjetoInventario;

    public ObjectsManager _objectsManager;
    public AudioSource _sound;
    public bool bConservar; //Con esta variable especificamos si queremos conservar el sprite en el inventario o no

    public float fSpeed;
    public ParticleSystem _particleSystem;

    private bool bUsed;
    private bool bObjetoInvetarioActivado = false;
    private Vector3 vDestino;

    void Awake()
    {
        goObjetoInventario.GetComponent<SpriteRenderer>().enabled = false;
        goObjetoInventario.GetComponent<Collider2D>().enabled = false;
    }

    void Start()
    {
        vDestino = goObjetoInventario.transform.position;
        bUsed = false;

        this.GetComponent<MoveToDestination>().setDestino(vDestino);
        this.GetComponent<MoveToDestination>().setSpeed(fSpeed);
    }

    void Update()
    {
        if(this.GetComponent<MoveToDestination>().getArrived() == true)
        {
            if (bConservar == false)
            {
                this.GetComponent<SpriteRenderer>().enabled = false;
                if((goObjetoInventario != null) && (bObjetoInvetarioActivado == false)) //La primera vez que se activa el objeto en el Inventario
                {
                    goObjetoInventario.GetComponent<SpriteRenderer>().enabled = true;
                    goObjetoInventario.GetComponent<Collider2D>().enabled = true;

                    _particleSystem.transform.position = goObjetoInventario.transform.position;
                    _particleSystem.Play();
                    bObjetoInvetarioActivado = true;
                }
            }
        }
    }

    void OnMouseDown()
    {
        if(bUsed == false)
        {
            this.GetComponent<MoveToDestination>().setMove(true);

            _objectsManager.IncrementObject();

            _sound.Play();

            bUsed = true;
        }
    }
}

