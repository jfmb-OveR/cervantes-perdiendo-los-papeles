﻿using UnityEngine;
using System.Collections;

public class SecundarioController : MonoBehaviour {
    public SpriteRenderer srSecundarioTriste;
    public SpriteRenderer srSecundarioContento;

    // Use this for initialization
    void Start () {
        setTriste();
    }

    public void setContento()
    {
        srSecundarioTriste.enabled = false;
        srSecundarioContento.enabled = true;
    }

    public void setTriste()
    {
        srSecundarioTriste.enabled = true;
        srSecundarioContento.enabled = false;
    }
}
