﻿using UnityEngine;
using System.Collections;

public class controlInventario : MonoBehaviour {
    [Header("True si es la primera escena")]
    public bool bFirstLoad;

    public GameObject[] objetosInventario;
    [Header("Activar/Desactivar objetos escena")]
    public bool bDesactivarObjetosEscena;
    public GameObject[] goObjectsScene;

    public static bool[] bArrayObjetosActivos;

    public static controlInventario inventarioRinconete;
    private int iNumObjetos;

    public void setActiveObject(int i)
    {
        bArrayObjetosActivos[i] = true;
    }

    public void setNotActiveObject(int i)
    {
        bArrayObjetosActivos[i] = false;
    }

    public void refreshActiveObjects()
    {
        for (int i = 0; i < iNumObjetos; i++)
        {
            bArrayObjetosActivos[i] = objetosInventario[i].GetComponent<SpriteRenderer>().enabled;

            Debug.Log("Objeto " + i + ": " + bArrayObjetosActivos[i]);
        }
    }

    // Use this for initialization
    void Awake()
    {
        iNumObjetos = objetosInventario.Length;

        inventarioRinconete = this;

        if (bArrayObjetosActivos == null)
        {
            bArrayObjetosActivos = new bool[iNumObjetos];

            for (int i = 0; i < iNumObjetos; i++)
            {
                bArrayObjetosActivos[i] = false;
            }
        }else
        {
            if (bFirstLoad == true)
            {
                bArrayObjetosActivos = new bool[iNumObjetos];
                for (int i = 0; i < iNumObjetos; i++)
                {
                    bArrayObjetosActivos[i] = false;
                }
            }
        }
    }


    void Start () {
        for (int i=0; i < iNumObjetos; i++)
        {
            objetosInventario[i].GetComponent<SpriteRenderer>().enabled = bArrayObjetosActivos[i];
            objetosInventario[i].GetComponent<BoxCollider2D>().enabled = bArrayObjetosActivos[i];

            if (bDesactivarObjetosEscena == true)
                goObjectsScene[i].SetActive(!bArrayObjetosActivos[i]);
        }
	}

}
