﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class myGameManager : MonoBehaviour {

    public static Dictionary<string, int> _myMap = new Dictionary<string, int>();
    public static Dictionary<int, string> _dicNombresEscenas = new Dictionary<int, string>();
    public static int _numEscenas = 13;
    public static string _sLanguage = "es";

    public static myGameManager myGameInstance;
	// Use this for initialization
	void Awake () {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        
        myGameInstance = this;

        if (_dicNombresEscenas.Count == 0)
        {
            _dicNombresEscenas.Add(0, "EscenaGalatea");
            _dicNombresEscenas.Add(1, "EscenaVidriera");
            _dicNombresEscenas.Add(2, "EscenaQuijote");
            _dicNombresEscenas.Add(3, "EscenaJauregui");
            _dicNombresEscenas.Add(4, "EscenaSancho");
            _dicNombresEscenas.Add(5, "EscenaCideHamete");
            _dicNombresEscenas.Add(6, "EscenaRoque");
            _dicNombresEscenas.Add(7, "EscenaMaritornes");
            _dicNombresEscenas.Add(8, "EscenaPasamonte");
            _dicNombresEscenas.Add(9, "EscenaCaballeros");
            _dicNombresEscenas.Add(10, "EscenaRinconete");
            _dicNombresEscenas.Add(11, "EscenaBarbero");
            _dicNombresEscenas.Add(12, "EscenaCipion");

            _myMap.Add("EscenaGalatea", PlayerPrefs.GetInt("EscenaGalatea"));
            _myMap.Add("EscenaVidriera", PlayerPrefs.GetInt("EscenaVidriera"));
            _myMap.Add("EscenaQuijote", PlayerPrefs.GetInt("EscenaQuijote"));
            _myMap.Add("EscenaJauregui", PlayerPrefs.GetInt("EscenaJauregui"));
            _myMap.Add("EscenaSancho", PlayerPrefs.GetInt("EscenaSancho"));
            _myMap.Add("EscenaCideHamete", PlayerPrefs.GetInt("EscenaCideHamete"));
            _myMap.Add("EscenaRoque", PlayerPrefs.GetInt("EscenaRoque"));
            _myMap.Add("EscenaMaritornes", PlayerPrefs.GetInt("EscenaMaritornes"));
            _myMap.Add("EscenaPasamonte", PlayerPrefs.GetInt("EscenaPasamonte"));
            _myMap.Add("EscenaCaballeros", PlayerPrefs.GetInt("EscenaCaballeros"));
            _myMap.Add("EscenaRinconete", PlayerPrefs.GetInt("EscenaRinconete"));
            _myMap.Add("EscenaBarbero", PlayerPrefs.GetInt("EscenaBarbero"));
            _myMap.Add("EscenaCipion", PlayerPrefs.GetInt("EscenaCipion"));


            //        for(int i = 0; i < _numEscenas; i++)
            //            Debug.Log("Escena: " + _dicNombresEscenas[i] + " Valor: " + _myMap[_dicNombresEscenas[i]]);
        }
        else
            refreshMap();
    }

    public void refreshMap()
    {
        for (int i = 0; i < _numEscenas; i++)
        {
            _myMap[_dicNombresEscenas[i]] = PlayerPrefs.GetInt(_dicNombresEscenas[i]);
        }
    }

    public bool gameOver()
    {
        bool bIsOver = true;
        for (int i = 0; (i < _numEscenas) && (bIsOver == true); i++)
        {
            Debug.Log("Valor de la escena " + i + ": " + _myMap[_dicNombresEscenas[i]]);
            if(_myMap[_dicNombresEscenas[i]] == 0)
                bIsOver = false;
        }

        return bIsOver;
    }

    public void updateSceneStatus(string sPlayerPrefName, int iNewValue)
    {
        PlayerPrefs.SetInt(sPlayerPrefName, iNewValue);
        _myMap[sPlayerPrefName] = iNewValue;
    }

    public void resetMap()
    {
        PlayerPrefs.SetInt("EscenaGalatea", 0);
        PlayerPrefs.SetInt("EscenaVidriera", 0);
        PlayerPrefs.SetInt("EscenaQuijote", 0);
        PlayerPrefs.SetInt("EscenaJauregui", 0);
        PlayerPrefs.SetInt("EscenaSancho", 0);
        PlayerPrefs.SetInt("EscenaCideHamete", 0);
        PlayerPrefs.SetInt("EscenaRoque", 0);
        PlayerPrefs.SetInt("EscenaMaritornes", 0);
        PlayerPrefs.SetInt("EscenaPasamonte", 0);
        PlayerPrefs.SetInt("EscenaCaballeros", 0);
        PlayerPrefs.SetInt("EscenaRinconete", 0);
        PlayerPrefs.SetInt("EscenaBarbero", 0);
        PlayerPrefs.SetInt("EscenaCipion", 0);
    }
}
