﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class CutSceneTextsManager : MonoBehaviour {

    public Text tCutScene1;
    public Text tCutScene2;
    public string sNombreCadena;
    [Header("True para Texto 1, False para Texto 2")]
    public bool bCheck;

    private LanguageManager _languageManager;
    // Use this for initialization
    void Awake() {
        _languageManager = LanguageManager.Instance;
        if (bCheck == true)
            tCutScene1.text = _languageManager.GetTextValue(sNombreCadena);
        else
            tCutScene2.text = _languageManager.GetTextValue(sNombreCadena);
    }

}
