﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SmartLocalization;

public class menu : MonoBehaviour {
    private myGameManager gameManager;
    [Header("Resetear las player prefs")]
    public bool bResetMenu;

    public Canvas quitMenu;
	public Button startText;
	public Button exitText;
    public Text textContinue;
    public Text textContinueDisabled;

    [Header("Canvas Créditos")]
    public Canvas canvasCredits;

    [Header("Texto de carga")]
    public Text textLoading;

    [Header("Script de traducción del main menu")]
    public MainMenuLanguageManager _languageManager;

    private bool bLoadScene = false;


    public void ExitPress() 
	{
	
		quitMenu.enabled = true;
		startText.enabled = false;
		exitText.enabled = false;
	}

	public void NoPress()
	{
		quitMenu.enabled = false;
		startText.enabled = true;
		exitText.enabled = true;

	}
	public void StartLevel()
	{
        PlayerPrefs.SetInt("JuegoEmpezado", 1);
        myGameManager.myGameInstance.resetMap();
        StartCoroutine(LoadScene(1));
//        Application.LoadLevel(1);
	}

    public void StartMapa()
    {
        StartCoroutine(LoadScene(2));
//        Application.LoadLevel(2);
    }

    public void StartMenu()
    {
        StartCoroutine(LoadScene(0));
//        Application.LoadLevel(0);
    }

    public void ExitGame()
	{
		Application.Quit ();	
	}

    public void showCredits()
    {
        canvasCredits.enabled = true;
    }

    public void exitCredits()
    {
        canvasCredits.enabled = false;
    }

    public void selectEnglish()
    {
        myGameManager._sLanguage = "en";
        LanguageManager.Instance.ChangeLanguage(myGameManager._sLanguage);
        _languageManager.setLanguageValues();
    }

    public void selectSpanish()
    {
        myGameManager._sLanguage = "es";
        LanguageManager.Instance.ChangeLanguage(myGameManager._sLanguage);
        _languageManager.setLanguageValues();
    }

    public void selectPortuguese()
    {
        myGameManager._sLanguage = "pt";
        LanguageManager.Instance.ChangeLanguage(myGameManager._sLanguage);
        _languageManager.setLanguageValues();
    }

    IEnumerator LoadScene(int newScene)
    {
        bLoadScene = true;
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }


    void Start()
    {

        canvasCredits.enabled = false;

        if (bResetMenu == true)
            PlayerPrefs.SetInt("JuegoEmpezado", 0);

        quitMenu = quitMenu.GetComponent<Canvas>();
        startText = startText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        textLoading.enabled = false;
        quitMenu.enabled = false;

        if (PlayerPrefs.GetInt("JuegoEmpezado") != 0)
        {
            textContinue.enabled = true;
            textContinueDisabled.enabled = false;
        }
        else
        {
            textContinue.enabled = false;
            textContinueDisabled.enabled = true;
        }

    }


    void Update()
    {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
        }
    }

}
