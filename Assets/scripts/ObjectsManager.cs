﻿using UnityEngine;
using System.Collections;

public class ObjectsManager : MonoBehaviour {

    public GameObject[] goObjetos;
    [Header("Número de objetos cogidos necesarios")]
    public int iMaxObjects = 0;

    [Header("¿Hay que activar un nuevo objeto en el inventario?")]
    public bool bNewObject = false;
    public GameObject _objetoInventario; //Objeto que se debe poner Enabled en el inventario
    public ParticleSystem _particulas;
    public AudioSource _sound;

    [Header("¿Hay que desactivar los objetos del inventario?")]
    public bool bDesactivarObjectos = false;

    private int iObjects = 0;
    private bool bObjectActivated = false;

    private bool bCollidersActivated;

    #region Getters
    public bool getCollidersActivated()
    {
        return bCollidersActivated;
    }

    public int getNumberOfObjects()
    {
        return iObjects;
    }

    public bool getObjectsEnded()
    {
        return (iObjects >= iMaxObjects);
    }
    #endregion

    public void IncrementObject()
    {
        iObjects++;
        Debug.Log("Número de objetos: " + iObjects);
    }

    public void desactivarColliders()
    {
        for (int i = 0; i < goObjetos.Length; i++)
        {
            goObjetos[i].GetComponent<BoxCollider2D>().enabled = false;
        }
        bCollidersActivated = false;
    }

    public void desactivarSprites()
    {
        for (int i = 0; i < goObjetos.Length; i++)
        {
            goObjetos[i].GetComponent<SpriteRenderer>().enabled = false;
            Debug.Log(i + " Sprite desactivado");
        }
    }

    public void activarColliders()
    {
        for (int i = 0; i < goObjetos.Length; i++)
        {
            goObjetos[i].GetComponent<BoxCollider2D>().enabled = true;
        }
        bCollidersActivated = true;
    }

    public void activarSprites()
    {
        for (int i = 0; i < goObjetos.Length; i++)
        {
            goObjetos[i].GetComponent<SpriteRenderer>().enabled = true;
            Debug.Log(i + " Sprite activado");
        }
    }

    void Update()
    {
        if(bNewObject == true)
        {
            if (getObjectsEnded() == true && bObjectActivated == false)
            {
                bObjectActivated = true;
                _objetoInventario.GetComponent<SpriteRenderer>().enabled = true;
                _objetoInventario.GetComponent<Collider2D>().enabled = true;
                _particulas.transform.position = _objetoInventario.transform.position;
                _particulas.Play();
                _sound.Play();

                if (bDesactivarObjectos == true)
                {
                    desactivarSprites();
                    //                desactivarColliders();
                }
            }
        }
    }
}
