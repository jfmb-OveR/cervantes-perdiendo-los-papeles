﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
//[RequireComponent(typeof(Rigidbody2D))]

public class venteroController : MonoBehaviour {
    public SceneManager _sceneManager;

    [Header("Animator del ventero")]
    public Animator anim;

    public AudioSource _soundError;
    public AudioSource _soundRonquidos;


    //    [Header("Sonidos del Ventero")]
    //    public AudioSource _soundGatoNormal;
    //    public AudioSource _soundGatoEnfadado;

    private enum State
    {
        IDLE,
        ANGRY,
        ANGRY2
    }

    private State state;

    private bool bIsAngry;
    private int AngryHash = Animator.StringToHash("isAngry");

    private bool bIsAngry2;
    private int Angry2Hash = Animator.StringToHash("isAngry2");

    private bool bAllowBroom = false;

    public bool getAllowBroom()
    {
        return bAllowBroom;
    }

    public void setAngryState()
    {
        StartCoroutine(setAngry());
    }

    public void setAngry2State()
    {
        StartCoroutine(setAngry2());
    }

    // Use this for initialization
    void Start()
    {
        state = State.IDLE;       
    }

    // Update is called once per frame
    void Update()
    {

        switch (state)
        {
            case State.IDLE:
                //                    Debug.Log("ESTADO IDLE");
//                _catMovement.setMove(false);
                bIsAngry = false;
                bIsAngry2 = false;
                break;
            case State.ANGRY:
                //                    Debug.Log("ESTADO WALKING   ");
                bIsAngry = true;
                bIsAngry2 = false;
                break;
            case State.ANGRY2:
                //                    Debug.Log("ESTADO ANGRY   ");
                bIsAngry = false;
                bIsAngry2 = true;
                break;
        }
        anim.SetBool(AngryHash, bIsAngry);
        anim.SetBool(Angry2Hash, bIsAngry2);
    }

    void OnTriggerStay2D(Collider2D other)
    {
//        Debug.Log(other.tag + "!!!!!!!");
        if(other.tag != "Untagged")
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                setAngryState();
            }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.tag + "!!!!!!!");
    }

    void OnMouseDown()
    {
        Debug.Log("He pulsado en el ventero");
        setAngryState();
    }

    IEnumerator setAngry()
    {
        _soundError.Play();
        _soundRonquidos.Stop();
        _sceneManager.IncrementErrors(1);
        state = State.ANGRY;
        yield return new WaitForSeconds(1);
        state = State.IDLE;
        _soundRonquidos.Play();
    }

    IEnumerator setAngry2()
    {
        state = State.ANGRY2;
        bAllowBroom = true;
        _soundRonquidos.Stop();
        yield return new WaitForSeconds(2);
        state = State.IDLE;
        _soundRonquidos.Play();
    }
}
