﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(MoveToDestination))]
public class rinconeteSecondScript : MonoBehaviour {
    public paperDetectionRinconete paperRinconete;
    [Header("Orden de los objetos: trapo, camisa, reloj")]
    public GameObject[] goSprites;

    public AudioSource[] aSonidos;
    public AudioSource _sonidoError;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    public SceneManager _scene;

    public SecundarioController _secundario;

    private LanguageManager _languageManager;
    private bool bReached = false;

    private bool[] bObjects;
    private bool bAllObjects;
    private int iNumObjects = 0;

    private bool bFirstTime = true;
    public bool getAllObjects()
    {
        return bAllObjects;
    }

    public void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag != "Paper")
        {
            //            Debug.Log("El objeto en colisión es: " + other.tag);
            if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
            {
                switch (other.tag)
                {
                    case "Trapo":
                        bObjects[0] = true;
                        goSprites[0].SetActive(false);
                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(goSprites[0]);
                        CheckAllObjects();
                        break;
                    case "Camisa":
                        bObjects[1] = true;
                        goSprites[1].SetActive(false);
                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(goSprites[1]);
                        CheckAllObjects();
                        break;
                    case "Reloj":
                        bObjects[2] = true;
                        goSprites[2].SetActive(false);
                        aSonidos[iNumObjects].Play();
                        iNumObjects++;
                        Destroy(goSprites[2]);
                        CheckAllObjects();
                        break;

                    default:
                        if (bReached == false)
                        {
                            StartCoroutine(ocultarError());
                        }
                        break;
                }
            }
        }
    }

    IEnumerator ocultarError()
    {
        _sonidoError.Play();
        bReached = true;
        _scene.IncrementErrors(1);
        switch (Random.Range(1, 6))
        {
            case 1:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                break;
            case 2:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                break;
            case 3:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                break;
            case 4:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                break;
            case 5:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                break;
        }
        iImagenError.enabled = true;
        tTextoError.enabled = true;
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    public bool CheckAllObjects()
    {
        bAllObjects = bObjects[0] && bObjects[1] && bObjects[2];
        //        Debug.Log("Valor de bAllObjects: " + bAllObjects);
        return (bAllObjects);
    }

    // Use this for initialization
    void Awake()
    {
        bAllObjects = false;
        bObjects = new bool[3];
        for (int i = 0; i < bObjects.Length; i++)
        {
            bObjects[i] = false;
        }

        _languageManager = LanguageManager.Instance;
    }

    void Update()
    {
        if (CheckAllObjects() == true)
        {
            if (bFirstTime == true)
            {
                bFirstTime = false;
                paperRinconete.moveObject();
            //            this.GetComponent<BoxCollider2D>().enabled = false;
            _secundario.setContento();
            }
        }
    }
}
