﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(BoxCollider2D))]
//[RequireComponent(typeof(Rigidbody2D))]
public class bellScript : MonoBehaviour {

    public venteroController ventero;
    public AudioSource audioBell;

    void OnMouseDown()
    {
        audioBell.Play();
        StartCoroutine(Angry());
    }

    IEnumerator Angry()
    {
        yield return new WaitForSeconds(0.25f);
        ventero.setAngryState();
    }
}
