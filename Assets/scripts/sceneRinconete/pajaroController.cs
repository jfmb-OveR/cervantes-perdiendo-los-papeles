﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

[RequireComponent(typeof(MoveToDestination))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class pajaroController : MonoBehaviour {
    public SceneManager _sceneManager;

    //    public RandomMovement _horseMovement;
    [Header("Configuración del pájaro")]
    public MoveToDestination _birdMovement;
    public GameObject goDestination;
    [Header("Tag del objeto válido")]
    public string sOtherTag;

    public float fSpeed;
    public AudioSource _sound;

    [Header("Reloj")]
    public BoxCollider2D colliderReloj;

    [Header("Animator del pajaro")]
    public Animator anim;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;
    public AudioSource _soundError;


    private bool bReached = false;
    private LanguageManager _languageManager;

    private enum State
    {
        IDLE,
        FLYING,
    }

    private State state;

    private bool bIsFlying;
    private int flyingHash = Animator.StringToHash("isFlying");

//    private int waypointIndex;
//    private int randomNumber;

    // Use this for initialization
    void Start()
    {
        _languageManager = LanguageManager.Instance;

        state = State.IDLE;

        _birdMovement.setDestino(goDestination.transform.position);
        _birdMovement.setSpeed(fSpeed);
        _birdMovement.setMove(false);
        _birdMovement.setKeepObject(true);

        colliderReloj.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (_birdMovement.getArrived() == true)
        {
            state = State.IDLE;
        }

        switch (state)
        {
            case State.IDLE:
                //                    Debug.Log("ESTADO IDLE");
                _birdMovement.setMove(false);
                bIsFlying = false;
                break;
            case State.FLYING:
                //                    Debug.Log("ESTADO FLYING   ");
                bIsFlying = true;
                break;

        }
        anim.SetBool(flyingHash, bIsFlying);
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (other.tag == sOtherTag)
            {
                if (bReached == false)
                {
//                    Destroy(other.gameObject);
                    StartCoroutine(birdFlying());
                }
            }
            else
            {
                if (bReached == false)
                {
                    bReached = true;
                    StartCoroutine(showError());
                }
            }
        }
    }

    void OnMouseDown()
    {
        StartCoroutine(showError());
    }


    IEnumerator showError()
    {
        _soundError.Play();
        _sceneManager.IncrementErrors(1);
        switch (Random.Range(1, 6))
        {
            case 1:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                break;
            case 2:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                break;
            case 3:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                break;
            case 4:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                break;
            case 5:
                tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                break;
        }
        iImagenError.enabled = true;
        tTextoError.enabled = true;

        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    IEnumerator birdFlying()
    {
        state = State.FLYING;
        yield return new WaitForSeconds(.5f);
        colliderReloj.enabled = true;
        _birdMovement.setMove(true);
    }
}
