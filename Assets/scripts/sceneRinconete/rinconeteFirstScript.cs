﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class rinconeteFirstScript : MonoBehaviour {
    public SceneManager _sceneManager;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;
    public AudioSource _soundError;

    private bool bReached = false;
    private LanguageManager _languageManager;

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (bReached == false)
            {
                StartCoroutine(showError());
                bReached = true;
            }
        }
    }

    IEnumerator showError()
    {
        _soundError.Play();
        _sceneManager.IncrementErrors(1);

        iImagenError.enabled = true;
        tTextoError.enabled = true;

        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }

    void Start()
    {
        _languageManager = LanguageManager.Instance;
        tTextoError.text = _languageManager.GetTextValue("Scene10.Error.Rinconete");
    }
}
