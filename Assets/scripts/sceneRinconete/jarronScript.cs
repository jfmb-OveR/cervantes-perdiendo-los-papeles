﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
//[RequireComponent(typeof(Rigidbody2D))]
public class jarronScript : MonoBehaviour {

    public venteroController ventero;
    public GameObject jarronRoto;
    public AudioSource audioJarronRoto;

    void Awake()
    {
        jarronRoto.SetActive(false);
    }

    void OnMouseDown()
    {
        ventero.setAngry2State();
        this.gameObject.SetActive(false);
        jarronRoto.SetActive(true);
        audioJarronRoto.Play();
    }
}
