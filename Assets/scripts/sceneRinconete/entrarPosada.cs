﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class entrarPosada : MonoBehaviour {
    public SceneManager _scene;
    public bool bCheckClock;
    public SpriteRenderer spriteClock;
    public AudioSource audioDoor;

    [Header("Siguiente escena")]
    public int iNewScene;

    [Header("Texto de carga")]
    public Text textLoading;

    private bool bLoadScene = false;

    void OnMouseDown()
    {
        audioDoor.Play();
        if (bCheckClock) //Es la escena interior
        {
            if (spriteClock.enabled == true)
            {
                controlInventario.inventarioRinconete.refreshActiveObjects();
                StartCoroutine(WaitBeforeLoading(1));
            }
            else
                Debug.Log("No está el reloj activo");
        }
        else //es la escena exterior
        {
            controlInventario.inventarioRinconete.refreshActiveObjects();
            StartCoroutine(WaitBeforeLoading(1));
        }
    }

    IEnumerator WaitBeforeLoading(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        _scene.setIsOverTrue();
    }

    public void LoadNewLevel()
    {
        bLoadScene = true;
        StartCoroutine(LoadScene(iNewScene));
    }

    IEnumerator LoadScene(int newScene)
    {
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }

    void Update()
    {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
        }
    }
}
