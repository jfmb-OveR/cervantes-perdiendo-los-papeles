﻿using UnityEngine;
using System.Collections;

public class paperDetectionRinconete : MonoBehaviour
{

//    [Header("Canvas")]
    public rinconeteSecondScript _jaureguiObjects;
    public SceneManager _sceneManager;
    public AudioSource _soundSuccess;
    public AudioSource _soundError;

    public MoveToDestination _moveController;
    public GameObject _destino;
    public float fSpeed;

    public void moveObject()
    {
        _moveController.setDestino(_destino.transform.position);
        _moveController.setSpeed(fSpeed);
        _moveController.setMove(true);

        _soundSuccess.Play();

        StartCoroutine(WaitBeforeLoading(1));
    }

    void OnMouseDown()
    {
        if (_jaureguiObjects.getAllObjects() == true)
        {
            moveObject();
        }
        else
        {
            _soundError.Play();
        }
    }

    IEnumerator WaitBeforeLoading(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        _sceneManager.setIsOverTrue();
    }

}
