﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class OpenDoor : MonoBehaviour {
    public SpriteRenderer puertaAbierta;
    public SpriteRenderer puertaCerrada;
    public GameObject _papel;
    public AudioSource _sonido;
    public AudioSource _sonidoError;
    public ObjectsManager _objectManager;

    [Header("Tag del objeto con el interactúa")]
    public string stringObjeto;

    [Header("Canvas Error")]
    public Image iImagenError;
    public Text tTextoError;

    public SceneManager _scene;

    private LanguageManager _languageManager;

    private bool bReached = false;

    void Awake()
    {
        _languageManager = LanguageManager.Instance;
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<DragDropScript>().getIsDropped() == true)
        {
            if (other.tag == stringObjeto)
            {
                if (bReached == false)
                {
                    _papel.GetComponent<Collider2D>().enabled = true;
                    puertaAbierta.enabled = true;
                    puertaCerrada.enabled = false;
                    this.GetComponent<BoxCollider2D>().enabled = false;
                    _objectManager.IncrementObject();
                    _sonido.Play();
                    //                Debug.Log("He soltado el objeto");
                    bReached = true;
                    Destroy(other.gameObject);
                    this.GetComponent<Collider2D>().enabled = false;
                }
            }
            else
            {
                if (bReached == false)
                {
                    _sonidoError.Play();
                    bReached = true;
                    _scene.IncrementErrors(1);
                    switch (Random.Range(1, 6))
                    {
                        case 1:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error01");
                            break;
                        case 2:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error02");
                            break;
                        case 3:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error03");
                            break;
                        case 4:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error04");
                            break;
                        case 5:
                            tTextoError.text = _languageManager.GetTextValue("Auxiliar.Texto.Error05");
                            break;
                    }
                    iImagenError.enabled = true;
                    tTextoError.enabled = true;
                    StartCoroutine(ocultarError());
                }
            }

        }
    }

    IEnumerator ocultarError()
    {
        yield return new WaitForSeconds(4);
        iImagenError.enabled = false;
        tTextoError.enabled = false;
        bReached = false;
    }
}
