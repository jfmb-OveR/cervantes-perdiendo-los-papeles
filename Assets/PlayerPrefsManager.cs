﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerPrefsManager : MonoBehaviour {
    [Header("Esta variable se usa para hacer pruebas")]
    public bool bResetMap;

    [Header("Imágenes para el mapa")]
    public Image imagenGalatea;
    public Image imagenVidriera;
    public Image imagenQuijote;
    public Image imagenJauregui;
    public Image imagenSancho;
    public Image imagenCideHamete;
    public Image imagenRoque;
    public Image imagenMaritornes;
    public Image imagenPasamonte;
    public Image imagenCaballeros;
    public Image imagenRinconete;
    public Image imagenBarbero;
    public Image imagenCipion;

    private Dictionary<int, bool> dicIntBool= new Dictionary<int, bool>();

	// Use this for initialization
	void Awake () {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        
        dicIntBool.Add(0, true);
        dicIntBool.Add(1, false);

        if (bResetMap == true)
        {
            PlayerPrefs.SetInt("EscenaGalatea", 0);
            PlayerPrefs.SetInt("EscenaVidriera", 0);
            PlayerPrefs.SetInt("EscenaQuijote", 0);
            PlayerPrefs.SetInt("EscenaJauregui", 0);
            PlayerPrefs.SetInt("EscenaSancho", 0);
            PlayerPrefs.SetInt("EscenaCideHamete", 0);
            PlayerPrefs.SetInt("EscenaRoque", 0);
            PlayerPrefs.SetInt("EscenaMaritornes", 0);
            PlayerPrefs.SetInt("EscenaPasamonte", 0);
            PlayerPrefs.SetInt("EscenaCaballeros", 0);
            PlayerPrefs.SetInt("EscenaRinconete", 0);
            PlayerPrefs.SetInt("EscenaBarbero", 0);
            PlayerPrefs.SetInt("EscenaCipion", 0);

            /*
                imagenGalatea.enabled = true;
                imagenVidriera.enabled = true;
                imagenQuijote.enabled = true;
                imagenJauregui.enabled = true;
                imagenSancho.enabled = true;
                imagenCideHamete.enabled = true;
                imagenRoque.enabled = true;
                imagenMaritornes.enabled = true;
                imagenPasamonte.enabled = true;
                imagenCaballeros.enabled = true;
                imagenRinconete.enabled = true;
                imagenBarbero.enabled = true;
                imagenCipion.enabled = true;
            */
        }
        else
        {
//            Debug.Log("La escena de Galatea está visitada?" + myGameManager._myMap[myGameManager._dicNombresEscenas[0]]);
//            Debug.Log("La escena de Galatea está visitada?" + dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[0]]]);

            myGameManager.myGameInstance.refreshMap();
            showImages();
/*            
                        if (PlayerPrefs.GetInt("EscenaGalatea") != 0)
                        {
                            imagenGalatea.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaVidriera") != 0)
                        {
                            imagenVidriera.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaQuijote") != 0)
                        {
                            imagenQuijote.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaJauregui") != 0)
                        {
                            imagenJauregui.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaSancho") != 0)
                        {
                            imagenSancho.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaCideHamete") != 0)
                        {
                            imagenCideHamete.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaRoque") != 0)
                        {
                            imagenRoque.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaMaritornes") != 0)
                        {
                            imagenMaritornes.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaPasamonte") != 0)
                        {
                            imagenPasamonte.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaCaballeros") != 0)
                        {
                            imagenCaballeros.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaRinconete") != 0)
                        {
                            imagenRinconete.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaBarbero") != 0)
                        {
                            imagenBarbero.enabled = false;
                        }
                        if (PlayerPrefs.GetInt("EscenaCipion") != 0)
                        {
                            imagenCipion.enabled = false;
                        }
                        */
        }

    }
    public void showImages()
    {
        imagenGalatea.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[0]]];
        imagenVidriera.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[1]]];
        imagenQuijote.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[2]]];
        imagenJauregui.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[3]]];
        imagenSancho.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[4]]];
        imagenCideHamete.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[5]]];
        imagenRoque.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[6]]];
        imagenMaritornes.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[7]]];
        imagenPasamonte.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[8]]];
        imagenCaballeros.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[9]]];
        imagenRinconete.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[10]]];
        imagenBarbero.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[11]]];
        imagenCipion.enabled = dicIntBool[myGameManager._myMap[myGameManager._dicNombresEscenas[12]]];
    }

}
