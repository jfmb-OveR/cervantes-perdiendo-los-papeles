﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MapEventManager : MonoBehaviour {

    [Header("Texto de carga")]
    public Text textLoading;

    private bool bLoadScene = false;

    public void sceneGalatea()
    {
        StartCoroutine(LoadScene(3));
        //        Application.LoadLevel(3);
    }

    public void sceneVidriera()
    {
        StartCoroutine(LoadScene(6));
        //Application.LoadLevel(6);
    }

    public void sceneQuijote()
    {
        StartCoroutine(LoadScene(11));
//        Application.LoadLevel(11);
    }

    public void sceneJauregui()
    {
        StartCoroutine(LoadScene(14));
//        Application.LoadLevel(14);
    }


    public void sceneSancho()
    {
        StartCoroutine(LoadScene(17));
//        Application.LoadLevel(17);
    }

    public void sceneCide()
    {
        StartCoroutine(LoadScene(20));
//        Application.LoadLevel(20);
    }

    public void sceneCaballeros()
    {
        StartCoroutine(LoadScene(23));
//        Application.LoadLevel(23);
    }

    public void sceneBarbero()
    {
        StartCoroutine(LoadScene(26));
//        Application.LoadLevel(25);
    }

    public void sceneMaritornes()
    {
        StartCoroutine(LoadScene(29));
        //        Application.LoadLevel(25);
    }

    public void sceneRinconete()
    {
        StartCoroutine(LoadScene(32));
        //        Application.LoadLevel(25);
    }

    public void sceneRoque()
    {
        StartCoroutine(LoadScene(37));
        //        StartCoroutine(LoadScene(29));
    }

    public void sceneGines()
    {
        StartCoroutine(LoadScene(40));
        //        StartCoroutine(LoadScene(29));
    }

    public void sceneCipion()
    {
        StartCoroutine(LoadScene(44));
    }


    // Use this for initialization
    void Start () {
        textLoading.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (bLoadScene == true)
        {
            textLoading.enabled = true;
            textLoading.color = new Color(textLoading.color.r, textLoading.color.g, textLoading.color.b, Mathf.PingPong(Time.time, 1));
        }
    }

    IEnumerator LoadScene(int newScene)
    {
        bLoadScene = true;
        AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(newScene);
        while (!async.isDone)
        {
            yield return null;
        }
    }

}
